module.exports = {
  apps: [
    {
      name: "is_api",
      script: "./dist/index.js",
      merge_logs: true,
      max_restarts: 20,
      instances: 4,
      exec_mode: 'cluster',
      max_memory_restart: "200M",
      env: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      }
    }
  ]
}
