import makeGetSurveys from  './getSurveys'
import makePostSurvey from './postSurvey'
import makeUpdateSurvey from './updateSurvey'
import makePatchSurvey from './patchSurvey'
import { listSurveys, getSurvey, createSurvey, updateSurvey, markSurveyPrimary  } from '../../use-cases'

const getSurveys = makeGetSurveys({
  getSurveys: listSurveys
})

const getSurveyDetails = makeGetSurveys({
  getSurveys: getSurvey
})

const postSurvey = makePostSurvey({
  postSurvey: createSurvey
})

const updateSurveyController = makeUpdateSurvey({
  updateSurvey
})

const patchSurveyController = makePatchSurvey({
  patchSurvey: updateSurvey,
  markSurveyPrimary
})

const isController = Object.freeze({
  getSurveys,
  getSurveyDetails,
  postSurvey,
  updateSurvey: updateSurveyController,
  patchSurvey: patchSurveyController
})

export default isController
export { getSurveys, getSurveyDetails, postSurvey, patchSurveyController as patchSurvey, updateSurveyController as updateSurvey }
