import httpStatus from 'http-status'

/**
 * Get survey
 */
const makeGetSurveys = ({ getSurvey }) => {
  return async (httpRequest) => {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const surveys = await getSurvey({
        surveyId: httpRequest.query.postId
      })
      return {
        headers,
        statusCode: httpStatus.OK,
        body: surveys
      }
    } catch (e) {
      // TODO: Error logging
      return {
        statusCode: httpStatus.BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}


export default makeGetSurveys
