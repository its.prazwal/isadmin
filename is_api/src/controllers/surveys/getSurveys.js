import httpStatus from 'http-status'

/**
 * Get survey list
 */
const makeGetSurveys = ({ getSurveys }) => {
  return async (httpRequest) => {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const surveys = await getSurveys({
        surveyId: httpRequest.params.id
      })
      return {
        headers,
        statusCode: httpStatus.OK,
        body: surveys
      }
    } catch (e) {
      // TODO: Error logging
      return {
        statusCode: httpStatus.BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}


export default makeGetSurveys
