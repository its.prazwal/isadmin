import httpStatus from 'http-status'

/**
 * Get User list
 */
const makeGetUsers = ({ getUsers }) => {
  return async () => {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const Users = await getUsers()
      return {
        headers,
        statusCode: httpStatus.OK,
        body: Users
      }
    } catch (e) {
      // TODO: Error logging
      return {
        statusCode: httpStatus.BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}


export default makeGetUsers
