import httpStatus from 'http-status'

/**
 * Get User Details
 */
const makeGetUserDetails = ({ getUser }) => {
  return async (request) => {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const user = await getUser(request.params.email)
      console.log('got user', user)
      return {
        headers,
        statusCode: httpStatus.OK,
        body: user
      }
    } catch (e) {
      // TODO: Error logging
      return {
        statusCode: httpStatus.BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}


export default makeGetUserDetails
