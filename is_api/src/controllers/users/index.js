import makeGetUsers from  './getUsers'
import makeGetUserDetails from './getUserDetails'
import { getUsers as getAllUsers, getUser as getUserDetails  } from '../../use-cases'

const getUsers = makeGetUsers({
  getUsers: getAllUsers
})

const getUser = makeGetUserDetails({
  getUser: getUserDetails
})


const isController = Object.freeze({
  getUser,
  getUsers,
})

export default isController
export { getUser, getUsers }
