import httpStatus from 'http-status'
import getPublicFields from '../../utils/compoundSurvey/compoundSurveyPublicFields'

/**
 * Create new compound survey
 */
const makePostCompoundSurvey = ({ postCompoundSurvey }) => {
  return async (httpRequest) => {
    try {
      const { source = {}, ...compoundSurveyInfo } = httpRequest.body
      source.ip = httpRequest.ip
      source.browser = httpRequest.headers['User-Agent']
      // eslint-disable-next-line dot-notation
      if (httpRequest.headers['Referer']) {
        // eslint-disable-next-line dot-notation
        source.referrer = httpRequest.headers['Referer']
      }
      const posted = await postCompoundSurvey({
        ...compoundSurveyInfo,
        source
      })

        console.log('data: ',compoundSurveyInfo)
      return {
        headers: {
          'Content-Type': 'application/json',
          'Last-Modified': new Date(posted.modifiedOn).toUTCString()
        },
        statusCode: httpStatus.CREATED,
        body: getPublicFields(posted)
      }
    } catch (e) {
      // TODO: Error logging
      console.log(e)

      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: httpStatus.BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}

export default makePostCompoundSurvey

