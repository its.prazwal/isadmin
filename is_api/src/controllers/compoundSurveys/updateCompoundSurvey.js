import httpStatus from 'http-status'
import getPublicFields from '../../utils/compoundSurvey/compoundSurveyPublicFields'


/**
 * Update existing compound survey
 */
const makeUpdateCompoundSurvey =  ({ updateCompoundSurvey }) => {
  return async (httpRequest) => {
    try {
      const { source = {}, ...surveyInfo } = httpRequest.body
      source.ip = httpRequest.ip
      source.browser = httpRequest.headers['User-Agent']
      // eslint-disable-next-line dot-notation
      if (httpRequest.headers['Referer']) {
        // eslint-disable-next-line dot-notation
        source.referrer = httpRequest.headers['Referer']
      }
      const toEdit = {
        ...surveyInfo,
        source,
        id: httpRequest.params.id
      }
      const updatedCompoundSurvey = await updateCompoundSurvey(toEdit)
      return {
        headers: {
          'Content-Type': 'application/json',
          'Last-Modified': new Date(updatedCompoundSurvey.modifiedOn).toUTCString()
        },
        statusCode: httpStatus.NO_CONTENT,
        body: getPublicFields(updatedCompoundSurvey)
      }
    } catch (e) {
      // TODO: Error logging
      console.log(e)
      return {
        headers: {
          'Content-Type': 'application/json'
        },
        statusCode: e.name === 'RangeError' ? httpStatus.NOT_FOUND : httpStatus.BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}


export default makeUpdateCompoundSurvey
