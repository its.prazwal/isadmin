import httpStatus from 'http-status'

/**
 * Get compound survey list
 */
const makeGetCompoundSurveys = ({ getCompoundSurveys }) => {
  return async (httpRequest) => {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const surveys = await getCompoundSurveys({
        surveyId: httpRequest.params.id
      })
      return {
        headers,
        statusCode: httpStatus.OK,
        body: surveys
      }
    } catch (e) {
      return {
        statusCode: httpStatus.BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}


export default makeGetCompoundSurveys
