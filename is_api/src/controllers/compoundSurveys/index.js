import makeGetCompoundSurveys from './getCompoundSurveys'
import makePostCompoundSurvey from './postCompoundSurvey'
import { listCompoundSurveys, getCompoundSurvey, createCompoundSurvey, updateCompoundSurvey  } from '../../use-cases'
import makeUpdateCompoundSurvey from "./updateCompoundSurvey";

const getCompoundSurveys = makeGetCompoundSurveys({
  getCompoundSurveys: listCompoundSurveys
})

const getCompoundSurveyDetails = makeGetCompoundSurveys({
  getCompoundSurveys: getCompoundSurvey
})

const postCompoundSurvey = makePostCompoundSurvey({
  postCompoundSurvey: createCompoundSurvey
})

const updateCompoundSurveyController = makeUpdateCompoundSurvey({
  updateCompoundSurvey
})

const isController = Object.freeze({
  getCompoundSurveys,
  getCompoundSurveyDetails,
  postCompoundSurvey,
  updateCompoundSurvey: updateCompoundSurveyController,
})

export default isController
export { getCompoundSurveys, getCompoundSurveyDetails, postCompoundSurvey, updateCompoundSurveyController as updateCompoundSurvey}
