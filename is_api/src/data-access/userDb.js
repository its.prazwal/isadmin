import _ from 'lodash'
import getPublicFields, { userPublicFields } from '../utils/user/userPublicFields'

const maxTimeout = 1000

const mapFieldsToProjection = fields => fields.reduce((o, k) => ({[k]: 1, ...o}), {})


const makeUserDb = ({ getDb }) => {
  const findAll = async () => {
    const db = await getDb()
    const result = await db.collection('users').find({}, mapFieldsToProjection(userPublicFields)).maxTimeMS(maxTimeout)
    return (await result.toArray()).map(({
      _id: id, ...user
    }) => ({
      id,
      ...getPublicFields(user)
    }))
  }

  const getUserDetails = async (email) => {
    const db = await getDb()
    const result = await db.collection('users').find({ email }).maxTimeMS(maxTimeout)
    const users = await result.toArray()
    console.log(email, 'users ', users)
    if (users.length === 0) {
      return null
    }
    const { _id: id, ...user } = users[0]
    return { id, ...user }
  }
  
  return Object.freeze({
    findAll,
    getUserDetails,
  })
}

export default makeUserDb
