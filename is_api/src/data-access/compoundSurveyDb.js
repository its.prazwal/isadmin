import Id from '../utils/Id'
import getPublicFields, { compoundSurveyPublicFields } from '../utils/compoundSurvey/compoundSurveyPublicFields'

const maxTimeout = 1000

const mapFieldsToProjection = fields => fields.reduce((o, k) => ({[k]: 1, ...o}), {})

const makeCompoundSurveyDb = ({ getDb }) => {
  const findAll = async () => {
    const db = await getDb()
    const result = await db.collection('compoundSurveys').find({}, mapFieldsToProjection(compoundSurveyPublicFields)).maxTimeMS(maxTimeout).toArray()
    return result.map(({
      _id: id, ...surveyDetails
    }) => ({
      id,
      ...getPublicFields(surveyDetails),
    }))
  }
  const findOneByBaseSurveyId = async ({baseSurveyId}) => {
    const db = await getDb()
    const result = await db.collection('compoundSurveys').find({baseSurveyId}).maxTimeMS(maxTimeout).toArray()
    return result[0]

  }
  const findById = async ({ id: _id }) => {
    const db = await getDb()
    const result = await db.collection('compoundSurveys').find({ _id }).maxTimeMS(maxTimeout)
    const compoundSurveys = await result.toArray()
    if (compoundSurveys.length === 0) {
      return null
    }
    const { _id: id, ...surveyDetails } = compoundSurveys[0]
    return { id, ...surveyDetails}
  }
  const insert = async ({ id: _id = Id.makeId(), ...surveyDetails }) => {
    const db = await getDb()
    const result = await db
      .collection('compoundSurveys')
      .insertOne({ _id, ...surveyDetails })
    const { _id: id, ...insertedInfo } = result.ops[0]
    return { id, ...insertedInfo }
  }

  const update = async ({ id: _id, ...surveyDetails }) => {
    const db = await getDb()
    const result = await db
        .collection('compoundSurveys')
        .updateOne({ _id }, { $set: { ...surveyDetails } })
    return result.modifiedCount > 0 ? { id: _id, ...surveyDetails } : null
  }
  return Object.freeze({
    findAll,
    findById,
    insert,
    update,
    findOneByBaseSurveyId,
  })
}

export default makeCompoundSurveyDb
