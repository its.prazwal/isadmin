import Id from '../utils/Id'
import getPublicFields, { surveyPublicFields } from '../utils/survey/surveyPublicFields'

const maxTimeout = 1000

const mapFieldsToProjection = fields => fields.reduce((o, k) => ({[k]: 1, ...o}), {})


const makeSurveyDb = ({ getDb }) => {
  const findAll = async () => {
    const db = await getDb()
    const result = await db.collection('surveys').find({}, mapFieldsToProjection(surveyPublicFields)).maxTimeMS(maxTimeout)
    return (await result.toArray()).map(({
      _id: id, ...surveyDetails
    }) => ({
      id,
      ...getPublicFields(surveyDetails),
      primary: !!surveyDetails.primary
    }))
  }
  const findById = async ({ id: _id }) => {
    const db = await getDb()
    const result = await db.collection('surveys').find({ _id }).maxTimeMS(maxTimeout)
    const surveys = await result.toArray()
    if (surveys.length === 0) {
      return null
    }
    const { _id: id, ...surveyDetails } = surveys[0]
    return { id, ...surveyDetails, primary: !!surveyDetails.primary }
  }
  const insert = async ({ id: _id = Id.makeId(), ...surveyDetails }) => {
    const db = await getDb()
    const result = await db
      .collection('surveys')
      .insertOne({ _id, ...surveyDetails })
    const { _id: id, ...insertedInfo } = result.ops[0]
    return { id, ...insertedInfo }
  }
  const update = async ({ id: _id, ...surveyDetails }) => {
    const db = await getDb()
    const result = await db
      .collection('surveys')
      .updateOne({ _id }, { $set: { ...surveyDetails } })
    return result.modifiedCount > 0 ? { id: _id, ...surveyDetails } : null
  }
  const markPrimary = async ({ id: _id, modifiedOn }) => {
    const db = await getDb()

    const result = await db.collection('surveys').updateMany(
      {},
      [
        {
          $set: {
            primary: {
              $switch: {
                branches: [
                  { case: { $eq: ["$_id", _id] }, then: true },
                ],
                default: false
              }
            }
          }
        }
      ]
    )

    return result.modifiedCount > 0 ? { id: _id, modifiedOn } : null
  }
  return Object.freeze({
    findAll,
    findById,
    markPrimary,
    insert,
    update,
  })
}

export default makeSurveyDb
