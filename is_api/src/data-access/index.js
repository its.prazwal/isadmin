import surveyDb from './surveyDb'
import compoundSurveyDb from "./compoundSurveyDb"
import makeUserDb from './userDb'
import dbService from '../config/db'

const { getDb } = dbService
const isDb = Object.freeze({
  userDb: makeUserDb({ getDb }),
  surveyDb: surveyDb({ getDb }),
  compoundSurveyDb: compoundSurveyDb({ getDb }),
})

export default isDb
