import buildUser from "../utils/user/user"

const makeGetUserDetails = ({ userDb }) => {
  return async (email) => {
    if (!email) {
      throw new Error('You must provide an email ID.')
    }
    const user = await userDb.getUserDetails(email)
    return buildUser(user, true)
  }
}

export default makeGetUserDetails
