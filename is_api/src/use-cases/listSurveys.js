const makeListSurveys = ({ surveyDb }) => {
  return async () => {
    console.time('survey find')
    const surveys = await surveyDb.findAll()
    console.timeEnd('survey find')
    return surveys
  }
}

export default makeListSurveys
