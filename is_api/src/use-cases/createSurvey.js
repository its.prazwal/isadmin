import makeSurvey from '../utils/survey'

const makeCreateSurvey = ({ surveyDb }) => {
  return async (surveyInfo) => {
    const survey = makeSurvey(surveyInfo)
    const exists = await surveyDb.findById({ id: survey.getId() })
    if (exists) {
      return exists
    }

    const surveySource = survey.getSource()
    return surveyDb.insert({
      title: survey.getTitle(),
      createdOn: survey.getCreatedOn(),
      hash: survey.getHash(),
      id: survey.getId(),
      modifiedOn: survey.getModifiedOn(),
      active: survey.isActive(),
      source: {
        ip: surveySource.getIp(),
        browser: surveySource.getBrowser(),
        referrer: surveySource.getReferrer()
      },
      takes: survey.getTakes(),
      pages: survey.getPages()
    })
  }
}

export default makeCreateSurvey
