import db from '../data-access'
import makeListSurveys  from './listSurveys'
import makeCreateSurvey from './createSurvey'
import makeUpdateSurvey from './updateSurvey'
import makeGetSurvey from './getSurvey'
import makePrimarySurvey from './makePrimarySurvey'
import makeGetUsers from './getUsers'
import makeGetUserDetails from './getUserDetails'
import makeGetCompoundSurvey from "./getCompoundSurvey"
import makeListCompoundSurveys from "./listCompoundSurveys"
import makeCreateCompoundSurvey from "./createCompoundSurvey"
import makeUpdateCompoundSurvey from "./updateCompoundSurvey";

const { surveyDb, userDb, compoundSurveyDb } = db
const listSurveys = makeListSurveys({surveyDb})
const getSurvey = makeGetSurvey({surveyDb})
const createSurvey = makeCreateSurvey({surveyDb})
const updateSurvey = makeUpdateSurvey({surveyDb, compoundSurveyDb})
const markSurveyPrimary = makePrimarySurvey({surveyDb})
const listCompoundSurveys = makeListCompoundSurveys({ compoundSurveyDb})
const getCompoundSurvey = makeGetCompoundSurvey({ compoundSurveyDb})
const createCompoundSurvey = makeCreateCompoundSurvey({ compoundSurveyDb})
const updateCompoundSurvey = makeUpdateCompoundSurvey({ compoundSurveyDb})
const getUsers = makeGetUsers({userDb})
const getUser = makeGetUserDetails({userDb})


const isService = Object.freeze({
  listSurveys,
  getSurvey,
  createSurvey,
  updateSurvey,
  markSurveyPrimary,
  getUsers,
  getUser,
  listCompoundSurveys,
  getCompoundSurvey,
  createCompoundSurvey,
  updateCompoundSurvey,
})

export default isService
export { listSurveys, createSurvey, updateSurvey, getSurvey, markSurveyPrimary, getUsers, getUser, listCompoundSurveys, getCompoundSurvey, createCompoundSurvey, updateCompoundSurvey}
