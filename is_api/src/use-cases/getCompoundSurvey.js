const makeGetCompoundSurvey =  ({ compoundSurveyDb }) => {
  return async ({ surveyId } = {}) => {
    if (!surveyId) {
      throw new Error('You must supply a survey ID.')
    }
    const compoundSurvey = await compoundSurveyDb.findById({
      id: surveyId,
    })
    return compoundSurvey
  }
}

export default makeGetCompoundSurvey
