const makeGetSurvey =  ({ surveyDb }) => {
  return async ({ surveyId } = {}) => {
    if (!surveyId) {
      throw new Error('You must supply a survey ID.')
    }
    const survey = await surveyDb.findById({
      id: surveyId,
    })
    return survey
  }
}

export default makeGetSurvey
