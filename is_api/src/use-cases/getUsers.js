import buildUser from "../utils/user/user"

const makeGetUsers = ({ userDb }) => {
  return async () => {
    const users = await userDb.findAll()
    return users.map(user => ({...buildUser(user)}))
  }
}

export default makeGetUsers
