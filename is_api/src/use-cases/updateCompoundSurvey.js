import { union, keys, has, assign } from 'lodash'
import makeCompoundSurvey from "../utils/compoundSurvey";

const makeUpdateCompoundSurvey = ({ compoundSurveyDb }) => {
  return async ({ id, ...changes } = {}) => {
    if (!id) {
      throw new Error('Invalid survey ID.')
    }

    const existing = await compoundSurveyDb.findById({ id })
    if (!existing) {
      throw new RangeError('Compound Survey not found.')
    }
    const hasOnlyUrlAccessible = union(keys(changes), ['urlAccessible', 'source']).length === 2
    if (existing.takes !== 0 && !hasOnlyUrlAccessible) {
      throw new Error('Cannot update a survey which is already in use')
    }
    if (!existing.active && hasOnlyUrlAccessible) {
      throw new Error('Cannot update a survey which is not active')
    }
    if (has(changes, 'active') && !changes.active) {
      assign(changes, { urlAccessible: false })
    }
    const compoundSurvey = makeCompoundSurvey({ ...existing, ...changes })
    return compoundSurveyDb.update({
      id: compoundSurvey.getId(),
      ...changes,
      modifiedOn: Date.now(),
    })
  }
}

export default makeUpdateCompoundSurvey
