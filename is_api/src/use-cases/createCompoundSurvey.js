import makeCompoundSurvey from '../utils/compoundSurvey'

const makeCreateCompoundSurvey = ({ compoundSurveyDb }) => {
  return async (surveyInfo) => {
    const compSurvey = makeCompoundSurvey(surveyInfo)
    const existing = await compoundSurveyDb.findById({ id: compSurvey.getId() })
    if (existing) {
      return existing
    }

    const surveySource = compSurvey.getSource()
    return compoundSurveyDb.insert({
      title: compSurvey.getTitle(),
      createdOn: compSurvey.getCreatedOn(),
      modifiedOn: compSurvey.getModifiedOn(),
      id: compSurvey.getId(),
      source: {
        ip: surveySource.getIp(),
        browser: surveySource.getBrowser(),
        referrer: surveySource.getReferrer()
      },
      takes: compSurvey.getTakes(),
      pages: compSurvey.getPages(),
      baseSurveyId: compSurvey.getBaseSurveyId(),
      active: compSurvey.isActive(),
      urlAccessible: compSurvey.isUrlAccessible()
    })
  }
}

export default makeCreateCompoundSurvey
