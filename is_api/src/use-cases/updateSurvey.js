import {has} from 'lodash'
import makeSurvey from '../utils/survey'

const makeUpdateSurvey = ({ surveyDb, compoundSurveyDb }) => {
  return async ({ id, ...changes } = {}) => {
    if (!id) {
      throw new Error('Invalid survey ID.')
    }

    const existing = await surveyDb.findById({ id })
    const isCompounded = await compoundSurveyDb.findOneByBaseSurveyId({baseSurveyId: id});
    console.log('somc: ',isCompounded)
    if (!existing) {
      throw new RangeError('Survey not found.')
    }
    if (existing.takes !== 0) {
      throw new Error('Cannot update a survey which is already in use')
    }
    if(has(changes, 'active') && !changes.active && isCompounded){
      throw new Error('Cannot update a survey which is linked to compound Survey')
    }
    const survey = makeSurvey({ ...existing, ...changes })
    const updatedSurvey = await surveyDb.update({
      id: survey.getId(),
      ...changes,
      modifiedOn: Date.now(),
    })
    return updatedSurvey
  }
}

export default makeUpdateSurvey
