const makePrimarySurvey = ({ surveyDb }) => {
  return async ({ id } = {}) => {
    if (!id) {
      throw new Error('Invalid survey ID.')
    }

    const existing = await surveyDb.findById({ id })
    if (!existing) {
      throw new RangeError('Survey not found.')
    }
    const updatedSurvey = await surveyDb.markPrimary({id})
    return updatedSurvey
  }
}

export default makePrimarySurvey
