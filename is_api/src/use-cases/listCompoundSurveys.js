const makeListCompoundSurveys = ({ compoundSurveyDb }) => {
  return async () => {
    console.time('Compound survey find')
    const compoundSurveys = await compoundSurveyDb.findAll()
    console.timeEnd('Compound survey find')
    return compoundSurveys
  }
}

export default makeListCompoundSurveys
