import crypto from 'crypto'
import ipRegex from 'ip-regex'

import Id from '../Id'
import buildMakeSource from '../source'
import buildMakeSurvey from './survey'


const isValidIp = ip => ipRegex({ exact: true }).test(ip)

const md5 = text => crypto
  .createHash('md5')
  .update(text, 'utf-8')
  .digest('hex')


const makeSource = buildMakeSource({ isValidIp })
const makeSurvey = buildMakeSurvey({
  Id, md5, makeSource 
})

export default makeSurvey

