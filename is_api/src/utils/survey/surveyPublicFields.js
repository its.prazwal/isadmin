import _ from 'lodash'

export const surveyPublicFields = ['id', 'title', 'hash', 'active', 'takes', 'modifiedOn', 'primary']

const getPublicFields = surveyObj => _.pick(surveyObj, surveyPublicFields)

export default getPublicFields
