const buildMakeSurvey =  ({
  Id, md5, makeSource 
}) => {
  const makeHash = (title, source, active) => md5(title, source + active)

  const makeSurvey = ({
    id = Id.makeId(),
    createdOn = Date.now(),
    modifiedOn = Date.now(),
    takes = 0,
    active = true,
    pages,
    source,
  } = {}) => {
    if (!Id.isValidId(id)) {
      throw new Error('Surveys must have a valid id.')
    }
    if (!pages || !pages.length) {
      throw new Error('Surveys must have atleast one page')
    }

    if (pages.some(page => !page.title)) {
      throw new Error('Title for the pages cannot be blank')
    }

    if (pages.some(page => !page.elements || page.elements.length === 0)) {
      throw new Error('Each page must have at least one valid question')
    }

    const validCreator = makeSource(source)
    let hash

    return Object.freeze({
      getTitle: () => pages[0].title,
      getCreatedOn: () => createdOn,
      getId: () => id,
      getModifiedOn: () => modifiedOn || Date.now(),
      getSource: () => validCreator,
      getTakes: () => takes,
      getPages: () => pages,
      isActive: () => active,
      activate: () => {
        active = true
      },
      deActivate: () => {
        active = false
      }
    })
  }

  return makeSurvey
}
export default buildMakeSurvey
