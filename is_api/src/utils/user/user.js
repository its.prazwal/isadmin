const buildUser = ({
  id,
  name,
  facebookId,
  googleId,
  surveyId,
  email,
  passwordGenerated,
  avatar
} = {}, authDetails = false) => {
  return Object.freeze({
    id: id,
    name,
    // eslint-disable-next-line no-nested-ternary
    registrationMethod: (facebookId ? 'Facebook' : (googleId ? 'Google' : 'Email')),
    surveyCompleted: surveyId,
    email,
    verified: passwordGenerated || !!facebookId || !!googleId,
    avatar: avatar,
    ...authDetails ? {
      facebookId,
      googleId,
    } : {}
  })
}

export default buildUser
