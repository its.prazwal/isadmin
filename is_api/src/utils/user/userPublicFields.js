import _ from 'lodash'

export const userPublicFields = ['googleId', 'facebookId', 'email', 'avatar', 'surveyId', 'name', 'passwordGenerated']

const getPublicFields = surveyObj => _.pick(surveyObj, userPublicFields)

export default getPublicFields
