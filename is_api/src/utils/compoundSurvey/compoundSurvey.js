const buildMakeCompoundSurvey =  ({
  Id, makeSource
}) => {
  const makeCompoundSurvey = ({
    id = Id.makeId(),
    createdOn = Date.now(),
    modifiedOn = Date.now(),
    pages,
    source,
    baseSurveyId,
    takes = 0,
    active = true,
    urlAccessible = true
  } = {}) => {
    if (!Id.isValidId(id)) {
      throw new Error('Surveys must have a valid id.')
    }
    if (!pages || !pages.length) {
      throw new Error('Surveys must have at least one page')
    }
    if (pages.some(page => !page.title)) {
      throw new Error('Title for the pages cannot be blank')
    }
    if (pages.some(page => !page.elements || page.elements.length === 0)) {
      throw new Error('Each page must have at least one valid question')
    }

    const validCreator = makeSource(source)

    return Object.freeze({
      getTitle: () => pages[0].title,
      getCreatedOn: () => createdOn,
      getModifiedOn: () => modifiedOn,
      getId: () => id,
      getSource: () => validCreator,
      getPages: () => pages,
      getBaseSurveyId: () => baseSurveyId,
      getTakes: () => takes,
      isActive: () => active,
      isUrlAccessible: () => urlAccessible,
    })
  }

  return makeCompoundSurvey
}
export default buildMakeCompoundSurvey
