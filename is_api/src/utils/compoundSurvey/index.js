import ipRegex from 'ip-regex'

import Id from '../Id'
import buildMakeSource from '../source'
import buildMakeCompoundSurvey from './compoundSurvey'

const isValidIp = ip => ipRegex({ exact: true }).test(ip)

const makeSource = buildMakeSource({ isValidIp })
const makeCompoundSurvey = buildMakeCompoundSurvey({
  Id, makeSource
})

export default makeCompoundSurvey

