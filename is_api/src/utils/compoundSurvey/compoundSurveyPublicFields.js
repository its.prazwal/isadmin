import _ from 'lodash'

export const compoundSurveyPublicFields = ['id', 'title', 'modifiedOn', 'takes', 'baseSurveyId', 'active', 'urlAccessible']

const getPublicFields = compoundSurveyObj => _.pick(compoundSurveyObj, compoundSurveyPublicFields)

export default getPublicFields
