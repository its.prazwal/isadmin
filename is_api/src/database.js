import dotenv from 'dotenv-safe'
import dbService from './config/db'

dotenv.config()
const initDB = async () => {
  console.log('Setting up database...')
  // database collection will automatically be created if it does not exist
  // indexes will only be added if they don't exist
  const db = await dbService.getDb()
  await db
    .collection('surveys')
    .createIndexes([
      {
        key: { primary: 1 }, name: 'primary', partialFilterExpression: { primary: { $eq: true } }
      },
      { key: { title: 1 }, name: 'title', unique: true }
    ], { unique: true })
  await db
    .collection('compoundSurveys')
    .createIndexes([
      { key: { title: 1 }, name: 'title', unique: true }
    ], { unique: true })
  console.log('Database setup complete...')
}
export default initDB
