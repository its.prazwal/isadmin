// make bluebird default Promise
// import Promise from 'bluebird'
// eslint-disable-line no-global-assign
import config from './config/vars'
import app from './config/express'
import initDB from './database'

// listen to requests
(async () => {
  await initDB()
  app.listen(config.port, () => console.info(`server started on port ${config.port} (${config.env})`))
})()

/**
 * Exports express
 * @public
 */
export default app
