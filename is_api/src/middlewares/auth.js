import jwt from 'express-jwt'
import jwksRsa from 'jwks-rsa'
import config from '../config/vars'

const checkJwt = jwt({
  secret: config.jwtKey,
  audience: config.authConfig.audience,
  issuer: `https://${config.authConfig.domain}/`,
  algorithm: ["RS256"]
})

exports.checkJwt = checkJwt

