import express from 'express'
import jwtAuthz from 'express-jwt-authz'

import { getUser, getUsers } from '../controllers/users'
import { checkJwt } from '../middlewares/auth'
import makeCallback from '../express-callback'

const router = express.Router()


router
  .route('/')
  .get(checkJwt, jwtAuthz(['view:users']), makeCallback(getUsers))


router
  .route('/:email')
  .get(checkJwt, jwtAuthz(['view:users']), makeCallback(getUser))



export default router
