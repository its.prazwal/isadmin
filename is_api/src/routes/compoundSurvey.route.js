import express from 'express'
import jwtAuthz from 'express-jwt-authz'

import makeCallback from '../express-callback'
import {
    getCompoundSurveys,
    getCompoundSurveyDetails,
    postCompoundSurvey,
    updateCompoundSurvey,
} from '../controllers/compoundSurveys'
import { checkJwt } from '../middlewares/auth'

const router = express.Router()

router
  .route('/')
  .get(checkJwt, jwtAuthz(['view:surveys']), makeCallback(getCompoundSurveys))
  .post(checkJwt, jwtAuthz(['create:surveys']), makeCallback(postCompoundSurvey))

router
  .route('/:id')
  .get(checkJwt, jwtAuthz(['view:surveys']), makeCallback(getCompoundSurveyDetails))
  .put(checkJwt, jwtAuthz(['create:surveys']), makeCallback(updateCompoundSurvey))

export default router
