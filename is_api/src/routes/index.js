import express from 'express'

import usersRoute from './users.route'
import surveyRoute from './survey.route'
import compoundSurveyRoute from './compoundSurvey.route'

const router = express.Router()

router.get('/status', (req, res) => res.send('OK'))

router.use('/users', usersRoute)
router.use('/surveys', surveyRoute)
router.use('/compoundSurveys', compoundSurveyRoute)

export default router
