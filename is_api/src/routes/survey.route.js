import express from 'express'
import jwtAuthz from 'express-jwt-authz'

import makeCallback from '../express-callback'
import { getSurveys, getSurveyDetails, postSurvey, patchSurvey, updateSurvey } from '../controllers/surveys'
import { checkJwt } from '../middlewares/auth'

const router = express.Router()

router
  .route('/')
  .get(checkJwt, jwtAuthz(['view:surveys']), makeCallback(getSurveys))
  .post(checkJwt, jwtAuthz(['create:surveys']), makeCallback(postSurvey))

router
  .route('/:id')
  .get(checkJwt, jwtAuthz(['view:surveys']), makeCallback(getSurveyDetails))
  .put(checkJwt, jwtAuthz(['create:surveys']), makeCallback(updateSurvey))
  .patch(checkJwt, jwtAuthz(['create:surveys']), makeCallback(patchSurvey))



export default router
