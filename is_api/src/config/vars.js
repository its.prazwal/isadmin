import path from 'path'
import dotenv from 'dotenv-safe'
import jwksRsa from 'jwks-rsa'

// import .env variables
dotenv.config({
  path: path.join(__dirname, process.env.NODE_ENV === 'local' ? `../../.env-local` : '../../.env'),
  sample: path.join(__dirname, '../../.env.example'),
})


export default {
  env: process.env.NODE_ENV,
  port: process.env.PORT || 3001,
  jwtSecret: process.env.JWT_SECRET || 'secret',
  jwtExpirationInterval: process.env.JWT_EXPIRATION_MINUTES,
  dbConfig: {
    dbName: process.env.DB_NAME,
    dbUser: process.env.DB_USER,
    dbPass: process.env.DB_PASS
  },
  authConfig: {
    domain: process.env.AUTH_DOMAIN,
    audience: process.env.AUTH_AUDIENCE
  },
  logs: process.env.NODE_ENV === 'production' ? 'combined' : 'dev',
  jwtKey: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${process.env.AUTH_DOMAIN}/.well-known/jwks.json`
  })

}
