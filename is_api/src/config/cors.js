const CORSConfig = {
  origin: ['http://localhost:3000', 'http://localhost:3002'],
  methods: "GET,HEAD,PUT,PATCH,POST",
  preflightContinue: false,
  optionsSuccessStatus: 204,
  maxAge: 600,
}

export default CORSConfig
