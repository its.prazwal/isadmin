export default function getPermissions(user) {
  return (user ? user[`${process.env.REACT_APP_AUTH_NAMESPACE}roles_and_permissions`].permissions : []);
}