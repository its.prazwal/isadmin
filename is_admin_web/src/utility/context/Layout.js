import React from 'react';

// eslint-disable-next-line import/no-cycle
import VerticalLayout from '../../layouts/VerticalLayout';
import FullLayout from '../../layouts/FullpageLayout';

const layouts = {
  vertical: VerticalLayout,
  full: FullLayout,
};

const ContextLayout = React.createContext();

const Layout = ({children}) => (<ContextLayout.Provider
  value={{
    state: {activeLayout: 'vertical'},
    fullLayout: layouts.full,
    VerticalLayout: layouts.vertical,
    switchDir: (dir) => {
      this.setState({ direction: dir });
    },
  }}
>
  {children}
</ContextLayout.Provider>)

export { Layout, ContextLayout };
