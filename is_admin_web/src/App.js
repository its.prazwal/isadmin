import React from 'react';

import AppRouter from './Router/AppRouter';
import './components/@IS/rippleButton/RippleButton';
import 'react-perfect-scrollbar/dist/css/styles.css';
import 'prismjs/themes/prism-tomorrow.css';


const App = () => <AppRouter />;

export default App;
