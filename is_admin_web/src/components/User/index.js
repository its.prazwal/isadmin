import React from 'react';
// import Spinner from '../../components/@IS/spinner/Loading-spinner';
import { Form, Label, Input, FormGroup, Col, Row } from 'reactstrap';

import LoadingSpinner from '../../components/@IS/spinner/Fallback-spinner';
import Avatar from '../@IS/avatar/AvatarComponent';
import Switch from '../@IS/switch';
import avatarColorGenerator from '../../configs/avatarColorGenerator';
import './User.scss'

const User = ({ user }) => {

  const { avatar, email, name, facebookId, googleId, registrationMethod, verified } = user


  const avatarProps = avatar ? { img: avatar } : { content: name? name.split(' ').map(a=>a.slice(0,1)) : email.slice(0, 2) }

  return (
    <>
      {user ? 
        <Form className="userdetails_form">
          <Row>
            <Col sm="4">
              <Avatar color={avatarColorGenerator()} size="xl" {...avatarProps}/>
            </Col>
            <Col sm="8">
              <Row>
                <Col sm="6">
                  <FormGroup>
                    <Label for="email">Email</Label>
                    <Input type="email" name="email" id="email" value={email} readOnly/>
                  </FormGroup>
                </Col>
                <Col sm="6">
                  <FormGroup>
                    <Label for="name">Name</Label>
                    <Input type="text" name="name" id="name" placeholder="user name" value={name} readOnly/>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col sm="6">
                  <FormGroup>
                    <Label for="facebookId">Facebook Id</Label>
                    <Input type="text" name="facebookId" id="facebookId" placeholder="" value={facebookId} readOnly/>
                  </FormGroup>
                </Col>
                <Col sm="6">
                  <FormGroup>
                    <Label for="googleId">Google Id</Label>
                    <Input type="text" name="googleId" id="googleId" placeholder="" value={googleId} readOnly/>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col sm="6">
                  <FormGroup>
                    <Label for="registrationMethod">Registration Method</Label>
                    <Input type="text" name="registrationMethod" id="registrationMethod" placeholder="" value={registrationMethod} readOnly/>
                  </FormGroup>
                </Col>
                <Col sm="6">
                  <FormGroup>
                    <Switch disabled checked={verified} label={verified ? 'Verified' : ''} />
                  </FormGroup>
                </Col>
              </Row>

            </Col>
          </Row>
        </Form>
        : <LoadingSpinner/>}
    </>
  );
};


export default User;