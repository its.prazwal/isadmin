import React from 'react';
import {
  Breadcrumb,
  BreadcrumbItem,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from 'reactstrap';
import { Home, Settings } from 'react-feather';
import { NavLink } from 'react-router-dom';


const BreadCrumbs = ({
  breadCrumbTitle, breadCrumbActive, breadCrumbParent, breadCrumbParent2, breadCrumbParent3,
}) => (
  <div className="content-header row">
    <div className="content-header-left col-md-9 col-12 mb-2">
      <div className="row breadcrumbs-top">
        <div className="col-12">
          {breadCrumbTitle ? (
            <h2 className="content-header-title float-left mb-0">
              {breadCrumbTitle}
            </h2>
          ) : (
            ''
          )}
          <div className="breadcrumb-wrapper vx-breadcrumbs d-sm-block d-none col-12">
            <Breadcrumb tag="ol">
              <BreadcrumbItem tag="li">
                <NavLink to="/">
                  <Home className="align-top" size={15} />
                </NavLink>
              </BreadcrumbItem>
              <BreadcrumbItem tag="li" className="text-primary">
                {breadCrumbParent}
              </BreadcrumbItem>
              {breadCrumbParent2 ? (
                <BreadcrumbItem tag="li" className="text-primary">
                  {breadCrumbParent2}
                </BreadcrumbItem>
              ) : (
                ''
              )}
              {breadCrumbParent3 ? (
                <BreadcrumbItem tag="li" className="text-primary">
                  {breadCrumbParent3}
                </BreadcrumbItem>
              ) : (
                ''
              )}
              <BreadcrumbItem tag="li" active>
                {breadCrumbActive}
              </BreadcrumbItem>
            </Breadcrumb>
          </div>
        </div>
      </div>
    </div>
    <div className="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
      <div className="form-group breadcrum-right dropdown">
        <UncontrolledButtonDropdown>
          <DropdownToggle
            color="primary"
            size="sm"
            className="btn-icon btn-round dropdown-toggle"
          >
            <Settings
              size={14}
              style={{
                left: 0,
              }}
            />
          </DropdownToggle>
          <DropdownMenu tag="ul" right>
            <DropdownItem tag="li">
              <NavLink className="text-dark w-100" to="/chat">
                Chat
              </NavLink>
            </DropdownItem>
            <DropdownItem tag="li">
              <NavLink className="text-dark w-100" to="/email/inbox">
                Email
              </NavLink>
            </DropdownItem>
            <DropdownItem tag="li">
              <NavLink className="text-dark w-100" to="/calendar">
                Calendar
              </NavLink>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledButtonDropdown>
      </div>
    </div>
  </div>
);

export default BreadCrumbs;
