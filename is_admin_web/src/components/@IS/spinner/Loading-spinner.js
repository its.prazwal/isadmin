import React from 'react';
import classnames from 'classnames';
import '../../../assets/scss/components/fallback-loader.scss';

const ComponentSpinner = ({ classes }) => (
  <div className={classnames('fallback-spinner', classes)}>
    <div className="fallback-loading component-loader">
      <div className="effect-1 effects" />
      <div className="effect-2 effects" />
      <div className="effect-3 effects" />
    </div>
  </div>
);

export default ComponentSpinner;
