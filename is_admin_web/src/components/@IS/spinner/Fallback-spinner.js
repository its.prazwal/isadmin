import React from 'react';

import logo from '../../../assets/img/logo/logo.png';
import '../../../assets/scss/components/app-loader.scss';

const SpinnerComponent = () => ((
  <div className="fallback-spinner fullwidth">
    <img className="fallback-logo" src={logo} alt="logo" />
    <div className="fallback-loading">
      <div className="effect-1 effects" />
      <div className="effect-2 effects" />
      <div className="effect-3 effects" />
    </div>
  </div>
));

export default SpinnerComponent;
