import React from 'react';

const CheckBoxesVuexy = ({
  className, defaultChecked, value, onClick, onChange, color, disabled, checked, icon, label, size,
}) => (
  <div
    className={`vx-checkbox-con ${
      className || ''
    } vx-checkbox-${color}`}
  >
    <input
      type="checkbox"
      defaultChecked={defaultChecked}
      checked={checked}
      value={value}
      disabled={disabled}
      onClick={onClick || null}
      onChange={onChange || null}
    />
    <span
      className={`vx-checkbox vx-checkbox-${
        size || 'md'
      }`}
    >
      <span className="vx-checkbox--check">{icon}</span>
    </span>
    <span>{label}</span>
  </div>
);
export default CheckBoxesVuexy;
