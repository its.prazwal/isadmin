import React from 'react';
import Toggle from 'react-toggle';
import 'react-toggle/style.css';
import '../../../assets/scss/plugins/forms/switch/react-toggle.scss';

const Switch = ({ checked, label, toggleHandler, disabled = false }) => (
  <div className="d-inline-block mr-1">
    <label className="react-toggle-wrapper">
      <Toggle checked={checked} onChange={toggleHandler} disabled={disabled}/>
      {label ? <span className="label-text">{label}</span> : null}
    </label>
  </div>
);

export default Switch;
