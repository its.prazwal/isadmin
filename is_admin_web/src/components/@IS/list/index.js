import React from 'react';
import DataTable from 'react-data-table-component';
import {
  ChevronDown,
} from 'react-feather';

import '../../../assets/scss/pages/data-list.scss';


const selectedStyle = {
  rows: {
    selectedHighlighStyle: {
      backgroundColor: 'rgba(115,103,240,.05)',
      color: '#7367F0 !important',
      boxShadow: '0 0 1px 0 #7367F0 !important',
      '&:hover': {
        transform: 'translateY(0px) !important',
      },
    },
  },
};

const List = ({ data, columnConfig, onRowClicked, noDataComponent="No Items" }) => (
  <div
    className="data-list"
  >
    <DataTable
      noDataComponent={noDataComponent}
      columns={columnConfig}
      data={data}
      noHeader
      responsive
      pointerOnHover
      onRowClicked={onRowClicked}
      customStyles={selectedStyle}
      sortIcon={<ChevronDown />}
    />

  </div>
);

export default List;
