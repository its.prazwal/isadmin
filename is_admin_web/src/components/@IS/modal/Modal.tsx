import React, { useState, FC, useEffect } from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';

interface ModalProps {
  isOpen?: boolean;
  header: string;
  unmountOnClose?: boolean;
  onClose?: () => void;
}

const ModalBasic: FC<ModalProps> = ({
  isOpen = false, children, header, unmountOnClose = false, onClose,
}) => {
  const [showModal, updateShowModal] = useState(isOpen);

  useEffect(() => {
    updateShowModal(isOpen);
  }, [isOpen]);

  const toggleModal = () => {
    updateShowModal(!showModal);
  };
  return (
    <Modal isOpen={showModal} onClosed={onClose} toggle={toggleModal} className="modal-dialog-centered modal-xl" unmountOnClose={unmountOnClose}>
      <ModalHeader toggle={toggleModal}>{header}</ModalHeader>
      <ModalBody className="modal-dialog-centered">
        {children}
      </ModalBody>
      <ModalFooter />
    </Modal>
  );
};
export default ModalBasic;
