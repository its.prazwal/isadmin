import React, {
    useState, Suspense, useEffect, lazy,
} from 'react';
import { Row } from 'reactstrap';
import Modal from '../../@IS/modal/Modal';
import Spinner from '../../@IS/spinner/Loading-spinner';
import LoadingSpinner from '../../@IS/spinner/Fallback-spinner';
import SurveyList from '../../@IS/list'
import CreateButton from '../CreateButton';
import { surveyType as SurveyType } from "../../../utility/constant/survey.constant";

const SurveyCreator = lazy(() => import('../Creator'));

const SurveysPageRender = ({baseSurveyId, showLoader, surveyList, surveyDetails, surveyColumnConfig, surveyRowClicked, surveyType, component = null
                 }) => {
    const [isSurveyCreatorVisible, updateIsSurveyCreatorVisible] = useState(false);
    const [surveyDetailsLocal, updateSurveyDetailsLocal] = useState(surveyDetails);

    const isCompoundSurvey = surveyType === SurveyType.COMPOUND;

    useEffect(() => {
        if (surveyDetails) {
            updateSurveyDetailsLocal(surveyDetails);
            updateIsSurveyCreatorVisible(true);
        }
    }, [surveyDetails]);

    const showSurveyCreator =  () => {
        updateSurveyDetailsLocal(null);
        updateIsSurveyCreatorVisible(true);
    };

    const hideSurveyCreator = () => {
        updateSurveyDetailsLocal(null);
        updateIsSurveyCreatorVisible(false);
    };

    return (
        <>
            {showLoader && surveyList ? <LoadingSpinner /> : null}
            <Row className="justify-content-end align-items-center">
                <CreateButton onClickAction={showSurveyCreator} btnText="New Survey" />
            </Row>
            <Modal isOpen={isSurveyCreatorVisible} header={surveyDetailsLocal ? isCompoundSurvey ? 'View Survey' : 'Update Survey' : 'Create Survey'} onClose={hideSurveyCreator}>
                {(component && !baseSurveyId && !surveyDetailsLocal) ? component :
                    <Suspense fallback={<Spinner />}>
                        <SurveyCreator surveyDetails={surveyDetailsLocal} surveyText={null} surveyType={surveyType} baseSurveyId={baseSurveyId} />
                    </Suspense>
                }
            </Modal>
            {!surveyList ? <Spinner />
                :<SurveyList
                    data={surveyList}
                    columnConfig={surveyColumnConfig}
                    onRowClicked={surveyRowClicked}
                    noDataComponent="No surveys created"
                />}
        </>
    );
}

export default SurveysPageRender;
