import React from 'react';
import { Button } from 'reactstrap';
import { Plus } from 'react-feather';

const CreateButton = ({ onClickAction, btnText }) => (
  <Button
    className="add-new-btn"
    color="primary"
    onClick={onClickAction}
  >
    <Plus size={15} />
    <span className="align-middle">{btnText}</span>
  </Button>
);

export default CreateButton;
