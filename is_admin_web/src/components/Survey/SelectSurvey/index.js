import React, { useEffect, useState } from 'react'
import { first } from 'lodash';
import { FormGroup, Label, Input, Col, Button  } from 'reactstrap';

const SelectSurvey = ({ surveys = [] , onSurveySelect }) => {
  const [selectedSurvey, setSelectedSurvey] = useState()

  useEffect(() => {
    setSelectedSurvey(first(surveys)?.id)
  }, [surveys]);

  const handleChange = ({ target: { value } }) => setSelectedSurvey(value)

  const handleSurveySelect = () => onSurveySelect(selectedSurvey)

  return(
    <div className="row justify-content-center align-items-center w-100">
      <FormGroup className="text-center w-50">
        <Label for="selectSurvey">Select Base Survey</Label>
        <Col className="mt-1 mb-1">
          <Input type="select" name="select" id="selectSurvey" onChange={handleChange} defaultValue={first(surveys)?.id || ''}>
            {surveys?.length ?
              surveys.map((survey) =>
                <option key={survey.id} value={survey.id}>{survey.title}</option>)
              : <option selected disabled>Select Base Survey</option>
            }
          </Input>
        </Col>
        <Button type="button" disabled={!surveys?.length} onClick={handleSurveySelect}>Select</Button>
      </FormGroup>
    </div>
  )
}

export default SelectSurvey;
