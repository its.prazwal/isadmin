import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import * as SurveyJSCreator from 'survey-creator';
import * as SurveyKo from 'survey-knockout';
import 'survey-creator/survey-creator.css';
import './creator.scss';
import 'nouislider/distribute/nouislider.css';
import 'select2/dist/css/select2.css';
import 'jquery-bar-rating/dist/themes/css-stars.css';
import 'jquery-bar-rating/dist/themes/fontawesome-stars.css';
// eslint-disable-next-line import/order
import $ from 'jquery';
import 'jquery-ui/ui/widgets/datepicker';
import 'select2/dist/js/select2';
import 'jquery-bar-rating';
import 'icheck/skins/square/blue.css';
import 'pretty-checkbox/dist/pretty-checkbox.css';

import * as widgets from 'surveyjs-widgets';

import { postSurvey, updateSurvey } from '../../../redux/surveys/actions';
import { postCompoundSurvey } from "../../../redux/compoundSurveys/actions";
import Spinner from '../../@IS/spinner/Fallback-spinner';
import { surveyType as SurveyType } from "../../../utility/constant/survey.constant"

SurveyJSCreator.StylesManager.applyTheme('default');

// widgets.icheck(SurveyKo, $);
widgets.prettycheckbox(SurveyKo);
widgets.select2(SurveyKo, $);
widgets.inputmask(SurveyKo);
widgets.jquerybarrating(SurveyKo, $);
widgets.jqueryuidatepicker(SurveyKo, $);
widgets.nouislider(SurveyKo);
widgets.select2tagbox(SurveyKo, $);
// widgets.signaturepad(SurveyKo);
widgets.sortablejs(SurveyKo);
widgets.ckeditor(SurveyKo);
widgets.autocomplete(SurveyKo, $);
widgets.bootstrapslider(SurveyKo);

const options = {
  showJSONEditorTab: true,
  showEmbededSurveyTab: false,
  questionTypes: ['text', 'checkbox', 'radiogroup', 'dropdown', 'rating', 'boolean'],
  isAutoSave: false,
  showPropertyGrid: false,
  showToolbox: 'right',
};

const SurveyCreator = ({
  showLoader, createSurvey, surveyDetails = null, updateSurveyDetails, surveyType = SurveyType.NORMAL, createCompSurvey, baseSurveyId,
}) => {
  const isCompoundAndViewOnly = surveyType === SurveyType.COMPOUND && surveyDetails
  const [surveyCreator] = useState(() => new SurveyJSCreator.SurveyCreator(
    null,
      {...options, readOnly: isCompoundAndViewOnly},
  ));

  const saveSurvey = () => {
    if (surveyType === SurveyType.NORMAL) {
      return (surveyDetails ? updateSurveyDetails : createSurvey)({ ...(surveyDetails || {}), ...JSON.parse(surveyCreator.text) });
    }
    return createCompSurvey({ ...JSON.parse(surveyCreator.text), baseSurveyId });
  }

  useEffect(() => {
    surveyCreator.render('surveyCreatorContainer');
  }, []);

  useEffect(() => {
    if(!isCompoundAndViewOnly){
      surveyCreator.saveSurveyFunc = saveSurvey;
    }
    surveyCreator.text = surveyDetails ? JSON.stringify({ ...surveyDetails }) : '';
  }, [surveyDetails]);

  return (
    <div className="survey-creator">
      {showLoader ? <Spinner /> : null}
      <div id="surveyCreatorContainer" />
    </div>
  );
};

const mapStateToProps = ({ ui: { loading } }) => ({
  showLoader: loading,
});

const mapDispatchToProps = {
  createSurvey: postSurvey,
  createCompSurvey: postCompoundSurvey,
  updateSurveyDetails: updateSurvey,
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyCreator);
