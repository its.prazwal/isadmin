import createAuth0Client from '@auth0/auth0-spa-js';

import { history } from '../history';

const AuthService = (() => {
  let client;
  const initOptions = {
    domain: process.env.REACT_APP_AUTH_DOMAIN,
    // eslint-disable-next-line @typescript-eslint/camelcase
    client_id: process.env.REACT_APP_AUTH_CLIENTID,
    useRefreshTokens: true,
    // eslint-disable-next-line @typescript-eslint/camelcase
    redirect_uri: window.location.origin,
  };
  return {
    onRedirectCallback: appState => {
      history.push(
        appState && appState.targetUrl
          ? appState.targetUrl
          : window.location.pathname,
      );
    },
    initAuthClient: async () => {
      client = await createAuth0Client(initOptions);
      return client;
    },
    getAuthClient: async () => {
      if (!client) {
        client = await createAuth0Client(initOptions);
      }
      return client;
    },
  };
})();

export default AuthService;
