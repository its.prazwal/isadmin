/* eslint-disable react/display-name */
import React from 'react';
import { Button } from 'reactstrap';

import Switch from '../components/@IS/switch';
import getFormattedDate from '../utility/date/format';
import './listColumnConfig.scss';


const makeSurveyListColumnConfig = ({ toggleSurveyActive, markPrimaryAction }) => ([
  {
    name: 'Title',
    selector: 'title',
    sortable: true,
    minWidth: '300px',
    ignoreRowClick: false,
    button: false,
    cell: row => (
      <p title={row.title} className="text-truncate text-bold-500 mb-0">
        {row.title}
      </p>
    ),
  },
  {
    name: 'Takes',
    selector: 'takes',
    sortable: true,
  },
  {
    name: 'Last Updated',
    selector: 'modifiedOn',
    sortable: true,
    cell: row => (
      <p title={row.title} className="text-truncate text-bold-500 mb-0">
        {row.modifiedOn ? getFormattedDate(row.modifiedOn) : '-'}
      </p>
    ),
  },
  {
    name: 'Active',
    cell: row => (
      <Switch checked={row.active} label={row.active ? 'Active' : 'Inactive'} toggleHandler={() => toggleSurveyActive(row)} />
    ),
  },
  {
    name: 'Primary',
    cell: row => (
      row.primary ? <span className="primary-cell">Primary</span>
      : <Button
        className="mark-primary-btn"
        color="primary"
        onClick={e => {
          e.stopPropagation();
          markPrimaryAction(row)
        }}
      >
        <span className="align-middle">Mark Primary</span>
      </Button>
    ),
  },
]);

export default makeSurveyListColumnConfig;
