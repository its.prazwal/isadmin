/* eslint-disable react/display-name */
import React from 'react';

import Avatar from '../components/@IS/avatar/AvatarComponent'
import avatarColorGenerator from './avatarColorGenerator';

import './listColumnConfig.scss';

const makeUserListColumnConfig = () => ([
  {
    name: 'Avatar',
    selector: 'avatar',
    sortable: true,
    minWidth: '200px',
    ignoreRowClick: false,
    cell: row => (
      <p title={row.avatar} className="text-truncate text-bold-500 mb-0">
        {row.avatar ? <Avatar color={avatarColorGenerator()} img={row.avatar}/> : <Avatar color={avatarColorGenerator()} content={row.name? row.name.split(' ').map(a=>a.slice(0,1)) : row.email.slice(0, 2)}/>}
      </p>
    ),
  },
  {
    name: 'Name',
    selector: 'name',
    sortable: true,
    ignoreRowClick: false,
    minWidth: '200px',
    cell: row => (
      <p title={row.name} className="text-truncate text-bold-500 mb-0">
        {row.name}
      </p>
    ),
  },
  {
    name: 'email',
    selector: 'email',
    sortable: true,
    ignoreRowClick: false,
    minWidth: '300px',
    cell: row => (
      <p title={row.email} className="text-truncate text-bold-500 mb-0">
        {row.email}
      </p>
    ),
  },
  {
    name: 'Survey',
    selector: 'surveyCompleted',
  },
  {
    name: 'Verified',
    selector: 'verified',
    cell: row => (
      <p title={row.verified}>
        {row.verified ? <span className="bullet bullet-success bullet-sm mr-1" /> : <span className="bullet bullet-secondary bullet-sm mr-1" />}
      </p>
    ),
  },
  {
    name: 'Registration',
    selector: 'registrationMethod',
    sortable: true,
    minWidth: '100px',
  },
]);

export default makeUserListColumnConfig;
