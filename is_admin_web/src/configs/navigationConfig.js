import React from 'react';
import * as Icon from 'react-feather';

import routes from '../Router/Routes';

const routeMap = [{
  title: 'home',
  icon: <Icon.Home size={20} />,
},
{
  title: 'surveys',
  icon: <Icon.TrendingUp size={20} />,
},
{
  title: 'compound surveys',
  icon: <Icon.TrendingUp size={20} />,
},
{
  title: 'users',
  icon: <Icon.Users size={20} />,
}];

const navigationConfig = routeMap.map(({ title, icon }) => {
  const route = routes.find(r => r.title.toLowerCase() === title);

  const { permissions, path: navLink } = route;
  return {
    id: title,
    title,
    permissions,
    navLink,
    type: 'item',
    icon,
  };
});

export default navigationConfig;
