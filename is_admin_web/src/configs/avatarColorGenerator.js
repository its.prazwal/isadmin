const avatarColors = [
  "primary",
  "success",
  "danger",
  "info",
  "warning",
  "dark",
]

const avatarColorGenerator = () => avatarColors[Math.floor(Math.random() * avatarColors.length)]

export default avatarColorGenerator