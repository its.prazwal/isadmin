/* eslint-disable react/display-name */
import React from 'react';

import getFormattedDate from '../utility/date/format';
import './listColumnConfig.scss';
import Switch from "../components/@IS/switch";


const makeCompoundSurveyListColConfig = ({ toggleCompoundSurveyActive, toggleCompoundSurveyUrlAccessible }) => ([
  {
    name: 'Title',
    selector: 'title',
    sortable: true,
    minWidth: '300px',
    ignoreRowClick: false,
    button: false,
    cell: row => (
      <p title={row.title} className="text-truncate text-bold-500 mb-0">
        {row.title}
      </p>
    ),
  },
  {
    name: 'Takes',
    selector: 'takes',
    sortable: true,
  },
  {
    name: 'Modified At',
    selector: 'modifiedOn',
    sortable: true,
    cell: row => (
      <p title={row.title} className="text-truncate text-bold-500 mb-0">
        {row.modifiedOn ? getFormattedDate(row.modifiedOn) : '-'}
      </p>
    ),
  },
  {
    name: 'Active',
    cell: row => (
        <Switch checked={row.active} toggleHandler={() => toggleCompoundSurveyActive(row)} />
    ),
  },
  {
    name: 'URL Accessible',
    cell: row => (
        <Switch checked={row.urlAccessible} toggleHandler={() => toggleCompoundSurveyUrlAccessible(row)} />
    ),
  },
  {
    name: 'Survey ID',
    selector: 'baseSurveyId',
    cell: row => (
      <>
        <p title={row.baseSurveyId} className="text-truncate text-bold-500 mb-0">
          {row.baseSurveyId || '-'}
        </p>
      </>
    ),
  },
]);

export default makeCompoundSurveyListColConfig;
