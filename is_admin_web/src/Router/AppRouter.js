import React from 'react';
import { Router, Switch } from 'react-router-dom';

import { history } from '../history';
import AppRoute from './AppRoute';
import Routes from './Routes';


const AppRouter = () => (
  // Set the directory path if you are deploying in sub-folder
  <Router history={history}>
    <Switch>
      {
        // eslint-disable-next-line react/no-array-index-key
        Routes.map((route, index) => <AppRoute key={index} {...route} />)
      }
    </Switch>
  </Router>
);


export default (AppRouter);
