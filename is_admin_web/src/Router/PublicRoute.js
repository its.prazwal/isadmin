import React from 'react';
import { Route } from 'react-router-dom';


const PublicRoute = ({
  renderComponent, ...rest
}) => {
  const render = props => renderComponent(props);

  return <Route render={render} {...rest} />;
};

export default PublicRoute;
