import React, { useEffect, useState } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { updateUser as updateUserAction } from '../redux/auth/actions';
import { homePath } from './Routes';
import SpinnerComponent from '../components/@IS/spinner/Fallback-spinner';
import AuthService from '../auth/AuthService';
import getPermissions from '../utility/user';


const PrivateRoute = ({
  user, updateUser, path, renderComponent, ...rest
}) => {
  const [loading, updateLoading] = useState(!user);
  const [userPermissions, updateUserPermissions] = useState(getPermissions(user));


  useEffect(() => {
    if (user) {
      updateUserPermissions(getPermissions(user));
      updateLoading(false);
      return;
    }
    const fn = async () => {
      const authClient = await AuthService.getAuthClient();

      if (window.location.search.includes('code=')
          && window.location.search.includes('state=')) {
        const { appState } = await authClient.handleRedirectCallback();
        AuthService.onRedirectCallback(appState);
      }

      const updatedIsAuthenticated = await authClient.isAuthenticated();


      if (updatedIsAuthenticated) {
        const updatedUser = await authClient.getUser();
        updateUser(updatedUser);
        return;
      }

      await authClient.loginWithRedirect({
        appState: { targetUrl: '/' },
        audience: process.env.REACT_APP_AUTH_AUDIENCE,
        scope: 'view:surveys create:surveys create:users view:users',
      });
    };
    fn();
  }, [user, loading, path]);

  const isAuthorized = !rest.permissions.some(permission => !userPermissions.includes(permission));

  const render = props => ((user && isAuthorized) ? renderComponent({ ...props, userPermissions })
    : (
      <Redirect
        to={{
          pathname: homePath.path,
          // eslint-disable-next-line react/destructuring-assignment
          state: { from: props.location },
        }}
      />
    ));
  return loading ? <SpinnerComponent /> : <Route render={render} {...rest} />;
};


const mapStateToProps = ({ auth: { user } }) => ({
  user,
});

const mapDispatchToProps = {
  updateUser: updateUserAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
