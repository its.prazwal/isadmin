import React, { Suspense } from 'react';

import Spinner from '../components/@IS/spinner/Fallback-spinner';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import VerticalLayout from '../layouts/VerticalLayout';

const RouterRenderProp = (Component, componentProps) => (
  <VerticalLayout {...componentProps}>
    <Suspense fallback={<Spinner />}>
      <Component {...componentProps} />
    </Suspense>
  </VerticalLayout>
);


// { secure, url, exact, component }
const AppRoute = ({
  component: Component,
  secure,
  fullLayout,
  ...rest
}) => (secure ? (
  <PrivateRoute
    renderComponent={(props) => RouterRenderProp(Component, { ...props, fullLayout })}
    {...rest}
  />
)
  : (
    <PublicRoute
      renderComponent={(props) => RouterRenderProp(Component, { ...props, fullLayout })}
      {...rest}
    />
  )
);
export default AppRoute;
