import { lazy } from 'react';

const Home = lazy(() => import('../views/pages/Home'));
const Surveys = lazy(() => import('../views/pages/Surveys'));
const CompoundSurveys = lazy(() => import('../views/pages/CompoundSurveys'));
const Users = lazy(() => import('../views/pages/Users'));


export const homePath = {
  path: '/', title: 'Home', exact: true, secure: true, component: Home, permissions: [],
};

export default [homePath, {
  path: '/surveys', exact: true, secure: true, title: 'Surveys', component: Surveys, permissions: ['view:surveys', 'create:surveys'],
},{
  path: '/compoundSurveys', exact: true, secure: true, title: 'Compound Surveys', component: CompoundSurveys, permissions: ['view:surveys', 'create:surveys'],
}, {
  path: '/users', exact: true, secure: true, title: 'Users', component: Users, permissions: ['view:users'],
}];
