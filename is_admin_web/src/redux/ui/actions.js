import {
  TOGGLE_LOADING,
} from './types';

// eslint-disable-next-line import/prefer-default-export
export const toggleLoadingState = () => ({
  type: TOGGLE_LOADING,
});
