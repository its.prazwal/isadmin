
import { TOGGLE_LOADING } from './types';
import createReducer from '../utils/createReducer';

const initialState = {
  loading: false,
};


const toggleLoadingState = createReducer(initialState)({
  [TOGGLE_LOADING]: state => ({
    ...state,
    loading: !state.loading,
  }),
});

export default toggleLoadingState;
