import { toast } from 'react-toastify';

import request from '../utils/request';
import { TOGGLE_LOADING } from '../ui/types';

const handleErrors = (error, action, next) => {
  toast.error(error.response ? error.response.data && error.response.data.error : 'Something went wrong');
  next({
    type: `${action.type}_FAILED`,
    payload: error,
    meta: action.meta,
  });

  return Promise.reject(error);
};

const handleResponse = (res, action, next) => {
  if(action.meta && action.meta.successMessage) {
    toast.success(action.meta.successMessage);
  }
  next({
    type: `${action.type}_COMPLETED`,
    payload: res,
    meta: action.meta,
  });

  return res;
};

const toggleLoading = (next) => {
  next({
    type: TOGGLE_LOADING,
  });
};

const dispatchChainedActions = (dispatch, action, res) => {
  if (action.meta.nextAction) {
    dispatch(action.meta.nextAction(res));
  }
};

const apiService = ({ dispatch }) => next => async action => {
  const result = next(action);
  if (!action.meta || !action.meta.async) {
    return result;
  }

  const {
    path, method = 'GET', body, scopes, params,
  } = action.meta;

  if (!path) {
    throw new Error(`'path' not specified for async action ${action.type}`);
  }

  toggleLoading(next);
  try {
    const res = await request({
      url: path, method, data: body, scopes, params,
    });
    toggleLoading(next);
    handleResponse(res, action, next);
    dispatchChainedActions(dispatch, action, res);
  } catch (error) {
    toggleLoading(next);
    handleErrors(error, action, next);
  }
};


export default apiService;
