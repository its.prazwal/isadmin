import {
  UPDATE,
} from './types';

// eslint-disable-next-line import/prefer-default-export
export const updateUser = userDetails => ({
  type: UPDATE,
  payload: userDetails,
});
