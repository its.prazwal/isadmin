import { UPDATE } from './types';
import createReducer from '../utils/createReducer';

const initialState = {
  user: null,
};


const cartReducer = createReducer(initialState)({
  [UPDATE]: (state, action) => ({
    ...state,
    user: action.payload,
  }),
});

export default cartReducer;
