import axios from 'axios';

import AuthService from '../../auth/AuthService';

const getToken = async (scopes: string[]) => {
  const authClient = await AuthService.getAuthClient();

  // eslint-disable-next-line no-return-await
  return await authClient.getTokenSilently({
    audience: process.env.REACT_APP_AUTH_AUDIENCE,
    scope: scopes.join(' '),
  });
};

const requestHeaders = async (scopes: string[]) => ({
  Accept: 'application/json',
  'Content-Type': 'application/json',
  Authorization: `Bearer ${await getToken(scopes)}`,
});

interface RequestConfig {
  url: string;
  method: 'get' | 'post' | 'delete' | 'put' | 'patch';
  data: {[key: string]: any};
  scopes: string[];
  params?: {[key: string]: string};
}

const request = async ({
  url,
  method,
  data = {},
  scopes,
  params,
}: RequestConfig) => {
  const headers = await requestHeaders(scopes);
  const requestConfig = {
    headers,
    method,
    url: process.env.REACT_APP_API_URL + url,
    data,
    ...(params && { params }),
  };

  try {
    const response = await axios.request(requestConfig);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export default request;
