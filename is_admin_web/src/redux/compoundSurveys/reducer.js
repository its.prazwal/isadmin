import {
  FETCH_COMPOUND_LIST_COMPLETED,
  POST_COMPOUND_SURVEY_COMPLETED,
  FETCH_COMPOUND_DETAILS_COMPLETED,
  UPDATE_COMPOUND_SURVEY_COMPLETED, VIEW_COMPOUND_SURVEY,
} from './types';
import createReducer from '../utils/createReducer';

export const initialState = {
  list: null,
  viewing: null,
};

export default createReducer(initialState)({
  [FETCH_COMPOUND_LIST_COMPLETED]: (state = initialState, action) => ({
    ...state,
    list: action.payload,
  }),
  [FETCH_COMPOUND_DETAILS_COMPLETED]: (state = initialState, action) => {
    const compoundSurveyList = state.list
    const currentRowIndex = state.list.findIndex(s => s.id === action.payload.id)
    return {
      ...state,
      list: currentRowIndex!==-1 ? [...compoundSurveyList.slice(0, currentRowIndex), action.payload, ...compoundSurveyList.slice(currentRowIndex+1)] : [...compoundSurveyList, action.payload],
    }
  },
  [VIEW_COMPOUND_SURVEY]: (state = initialState, action) => ({
    ...state,
    viewing: action.payload,
  }),
  [POST_COMPOUND_SURVEY_COMPLETED]: (state = initialState, action) => ({
    ...state,
    posted: action.payload,
  }),
  [UPDATE_COMPOUND_SURVEY_COMPLETED]: (state = initialState, action) => ({
    ...state,
    updated: action.payload,
  }),
});
