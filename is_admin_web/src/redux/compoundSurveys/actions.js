import {
  FETCH_COMPOUND_LIST,
  POST_COMPOUND_SURVEY,
  FETCH_COMPOUND_DETAILS,
    UPDATE_COMPOUND_SURVEY,
    VIEW_COMPOUND_SURVEY
} from './types';

export const fetchCompoundSurveys = () => ({
  type: FETCH_COMPOUND_LIST,
  meta: {
    async: true,
    blocking: true,
    path: 'compoundSurveys',
    method: 'GET',
    scopes: ['view:surveys'],
  },
});

export const fetchCompoundSurvey = data => ({
  type: FETCH_COMPOUND_DETAILS,
  meta: {
    async: true,
    blocking: true,
    path: `compoundSurveys/${data.id}`,
    method: 'GET',
    scopes: ['view:surveys'],
  },
});

export const fetchAndViewCompoundSurvey = data => ({
  type: FETCH_COMPOUND_DETAILS,
  meta: {
    async: true,
    blocking: true,
    path: `compoundSurveys/${data.id}`,
    method: 'GET',
    scopes: ['view:surveys'],
    nextAction: (fetchedData) => viewCompoundSurvey(fetchedData)
  },
});

export const viewCompoundSurvey = data => ({
  type: VIEW_COMPOUND_SURVEY,
  payload: data,
});

export const postCompoundSurvey = data => ({
  type: POST_COMPOUND_SURVEY,
  meta: {
    async: true,
    blocking: true,
    path: 'compoundSurveys',
    method: 'POST',
    body: data,
    scopes: ['create:surveys'],
    nextAction: posted => fetchCompoundSurvey({ id: posted.id }),
    successMessage: 'Survey Created',
  },
});

export const updateCompoundSurvey = data => ({
  type: UPDATE_COMPOUND_SURVEY,
  meta: {
    async: true,
    blocking: true,
    path: `compoundSurveys/${data.id}`,
    method: 'PUT',
    body: data,
    scopes: ['create:surveys'],
    nextAction: () => fetchCompoundSurvey({ id: data.id }),
    successMessage: 'Compound Survey Updated',
  },
});
