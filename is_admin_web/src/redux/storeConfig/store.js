import { createStore, applyMiddleware, compose } from 'redux';
import createDebounce from 'redux-debounced';
import thunk from 'redux-thunk';

import rootReducer from '../rootReducer';
import apiService from '../middlewares/API';

const middlewares = [thunk, apiService, createDebounce()];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  {},
  composeEnhancers(applyMiddleware(...middlewares)),
);

// eslint-disable-next-line import/prefer-default-export
export { store };
