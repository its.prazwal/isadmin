export const FETCH_LIST = 'surveys/FETCH_LIST';
export const FETCH_LIST_COMPLETED = 'surveys/FETCH_LIST_COMPLETED';
export const FETCH_LIST_FAILED = 'surveys/FETCH_LIST_FAILED';

export const FETCH_DETAILS = 'surveys/FETCH_DETAILS';
export const FETCH_DETAILS_COMPLETED = 'surveys/FETCH_DETAILS_COMPLETED';
export const FETCH_DETAILS_FAILED = 'surveys/FETCH_DETAILS_FAILED';

export const POST_SURVEY = 'surveys/POST_SURVEY';
export const POST_SURVEY_COMPLETED = 'surveys/POST_SURVEY_COMPLETED';
export const POST_SURVEY_FAILED = 'surveys/POST_SURVEY_FAILED';

export const UPDATE_SURVEY = 'surveys/UPDATE_SURVEY';
export const UPDATE_SURVEY_COMPLETED = 'surveys/UPDATE_SURVEY_COMPLETED';
export const UPDATE_SURVEY_FAILED = 'surveys/UPDATE_SURVEY_FAILED';

export const MARK_PRIMARY = 'surveys/MARK_PRIMARY';
export const MARK_PRIMARY_COMPLETED = 'surveys/MARK_PRIMARY_COMPLETED';
export const MARK_PRIMARY_FAILED = 'surveys/MARK_PRIMARY_FAILED';

export const EDIT_SURVEY = 'surveys/EDIT_SURVEY';
