import {
  FETCH_LIST,
  POST_SURVEY,
  UPDATE_SURVEY,
  FETCH_DETAILS,
  EDIT_SURVEY,
  MARK_PRIMARY,
} from './types';

export const fetchSurveys = () => ({
  type: FETCH_LIST,
  meta: {
    async: true,
    blocking: true,
    path: 'surveys',
    method: 'GET',
    scopes: ['view:surveys'],
  },
});

export const editSurvey = data => ({
  type: EDIT_SURVEY,
  payload: data,
});

export const fetchAndEditSurvey = data => ({
  type: FETCH_DETAILS,
  meta: {
    async: true,
    blocking: true,
    path: `surveys/${data.id}`,
    method: 'GET',
    scopes: ['view:surveys'],
    nextAction: (fetchedData) => editSurvey(fetchedData),
  },
});

export const fetchSurvey = data => ({
  type: FETCH_DETAILS,
  meta: {
    async: true,
    blocking: true,
    path: `surveys/${data.id}`,
    method: 'GET',
    scopes: ['view:surveys'],
  },
});

export const postSurvey = data => ({
  type: POST_SURVEY,
  meta: {
    async: true,
    blocking: true,
    path: 'surveys',
    method: 'POST',
    body: data,
    scopes: ['create:surveys'],
    nextAction: posted => fetchSurvey({ id: posted.id }),
    successMessage: 'Survey Created',
  },
});

export const patchSurvey = data => ({
  type: UPDATE_SURVEY,
  meta: {
    async: true,
    blocking: true,
    path: `surveys/${data.id}`,
    method: 'PATCH',
    body: data,
    scopes: ['create:surveys'],
    nextAction: () => fetchSurvey({ id: data.id }),
    successMessage: 'Survey Updated',
  },
});

export const markPrimary = data => ({
  type: MARK_PRIMARY,
  meta: {
    async: true,
    blocking: true,
    path: `surveys/${data.id}`,
    method: 'PATCH',
    body: data,
    scopes: ['create:surveys'],
    nextAction: fetchSurveys,
    successMessage: 'Survey Updated',
  },
});

export const updateSurvey = data => ({
  type: UPDATE_SURVEY,
  meta: {
    async: true,
    blocking: true,
    path: `surveys/${data.id}`,
    method: 'PUT',
    body: data,
    scopes: ['create:surveys'],
    nextAction: () => fetchSurvey({ id: data.id }),
    successMessage: 'Survey Updated',
  },
});
