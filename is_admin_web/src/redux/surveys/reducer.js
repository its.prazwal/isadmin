import {
  FETCH_LIST_COMPLETED, POST_SURVEY_COMPLETED, UPDATE_SURVEY_COMPLETED, FETCH_DETAILS_COMPLETED, EDIT_SURVEY,
} from './types';
import createReducer from '../utils/createReducer';

export const initialState = {
  list: null,
  editing: null,
};

export default createReducer(initialState)({
  [FETCH_LIST_COMPLETED]: (state = initialState, action) => ({
    ...state,
    list: action.payload,
    // reset editing
    editing: null,
  }),
  [FETCH_DETAILS_COMPLETED]: (state = initialState, action) => {
    const surveyList = state.list
    const currentRowIndex = state.list.findIndex(s=>s.id===action.payload.id)
    return {
      ...state,
      list: currentRowIndex!==-1 ? [...surveyList.slice(0, currentRowIndex), action.payload, ...surveyList.slice(currentRowIndex+1)] : [...surveyList, action.payload],
    }
  },
  [EDIT_SURVEY]: (state = initialState, action) => ({
    ...state,
    editing: action.payload,
  }),
  [POST_SURVEY_COMPLETED]: (state = initialState, action) => ({
    ...state,
    posted: action.payload,
  }),
  [UPDATE_SURVEY_COMPLETED]: (state = initialState, action) => ({
    ...state,
    updated: action.payload,
  }),
});
