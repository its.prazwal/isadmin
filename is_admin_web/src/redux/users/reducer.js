import {
  FETCH_LIST_COMPLETED,
  FETCH_DETAILS_COMPLETED,
} from './types';
import createReducer from '../utils/createReducer';

export const initialState = {
  list: [],
  selected: null,
};

export default createReducer(initialState)({
  [FETCH_LIST_COMPLETED]: (state = initialState, action) => ({
    ...state,
    list: action.payload,
    // reset selected
    selected: null,
  }),
  [FETCH_DETAILS_COMPLETED]: (state = initialState, action) => {
    const userList = state.list
    console.log('list', state.list)
    const currentIndex = state.list.findIndex(s=>s.id===action.payload.id)
    return {
      ...state,
      list: currentIndex!==-1 ? [...userList.slice(0, currentIndex), action.payload, ...userList.slice(currentIndex+1)] : [...userList, action.payload],
      selected: action.payload,
    }
  },
});
