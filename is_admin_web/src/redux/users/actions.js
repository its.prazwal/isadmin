import {
  FETCH_LIST,
  FETCH_DETAILS,
} from './types';

export const fetchUsers = () => ({
  type: FETCH_LIST,
  meta: {
    async: true,
    blocking: true,
    path: 'users',
    method: 'GET',
    scopes: ['view:users'],
  },
});

export const fetchUserDetails = email => ({
  type: FETCH_DETAILS,
  meta: {
    async: true,
    blocking: true,
    path: `users/${email}`,
    method: 'GET',
    scopes: ['view:users'],
  },
});