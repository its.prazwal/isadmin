import themeConfig from '../../../configs/themeConfig';
import createReducer from '../../utils/createReducer';


export default createReducer(themeConfig)({
  CHANGE_MODE: (state, action) => ({ ...state, theme: action.mode }),
  COLLAPSE_SIDEBAR: (state, action) => ({ ...state, sidebarCollapsed: action.value }),
  CHANGE_NAVBAR_COLOR: (state, action) => ({ ...state, navbarColor: action.color }),
  CHANGE_NAVBAR_TYPE: (state, action) => ({ ...state, navbarType: action.style }),
  CHANGE_FOOTER_TYPE: (state, action) => ({ ...state, footerType: action.style }),
  CHANGE_MENU_COLOR: (state, action) => ({ ...state, menuTheme: action.style }),
  HIDE_SCROLL_TO_TOP: (state, action) => ({ ...state, hideScrollToTop: action.value }),
});
