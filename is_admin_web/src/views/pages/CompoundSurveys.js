import React, {useState, useEffect} from 'react';
import { connect } from 'react-redux';
import {
  fetchCompoundSurveys,
  updateCompoundSurvey,
  fetchAndViewCompoundSurvey
} from '../../redux/compoundSurveys/actions'
import makeCompoundSurveyListColConfig from '../../configs/compoundSurveyListColConfig';
import {fetchSurveys} from "../../redux/surveys/actions";
import SurveysPageRender from "../../components/Survey/Survey";
import SelectSurvey from "../../components/Survey/SelectSurvey";

const CompoundSurveys = ({showLoader,
                   getCompoundSurveys,
                   getSurveyList,
                   baseSurveyList,
                   compoundList,
                   compoundSurveyDetails,
                   toggleCompoundSurveyUrlAccessible, toggleCompoundSurveyActive, fetchCompoundSurveyDetails
}) => {

  const [baseSurveyId, setBaseSurveyId] = useState();

  useEffect(() => {
      getCompoundSurveys();
    getSurveyList();
  }, []);

  const handleSelect = (value) => {
    setBaseSurveyId(value);
  }

  const selectSurvey =  <SelectSurvey surveys={baseSurveyList} onSurveySelect={handleSelect} />


  return (
      <SurveysPageRender
          surveyColumnConfig={makeCompoundSurveyListColConfig({toggleCompoundSurveyActive, toggleCompoundSurveyUrlAccessible})}
          surveyDetails={compoundSurveyDetails}
          surveyList={compoundList}
          surveyType="COMPOUND"
          surveyRowClicked={fetchCompoundSurveyDetails}
          component={selectSurvey}
          baseSurveyId={baseSurveyId}
          showLoader={showLoader}
      />
  );
};

const mapStateToProps = ({ui: { loading }, compoundSurveys: { list: compoundList, viewing }, surveys: { list } }) => ({
  compoundList,
  showLoader: loading,
  baseSurveyList: list,
  compoundSurveyDetails: viewing,
});

const mapDispatchToProps = dispatch => ({
  getCompoundSurveys: () => dispatch(fetchCompoundSurveys()),
  getSurveyList: () => dispatch(fetchSurveys()),
  toggleCompoundSurveyActive: (compoundSurveyDetails) => dispatch(updateCompoundSurvey({ id: compoundSurveyDetails.id, active: !compoundSurveyDetails.active })),
  toggleCompoundSurveyUrlAccessible: (compoundSurveyDetails) => dispatch(updateCompoundSurvey({ id: compoundSurveyDetails.id, urlAccessible: !compoundSurveyDetails.urlAccessible })),
  fetchCompoundSurveyDetails: (compoundSurveyDetails) => dispatch(fetchAndViewCompoundSurvey({ id: compoundSurveyDetails.id })),
});

export default connect(mapStateToProps, mapDispatchToProps)(CompoundSurveys);
