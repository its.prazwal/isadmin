import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { fetchSurveys, patchSurvey, fetchAndEditSurvey, markPrimary } from '../../redux/surveys/actions';
import makeSurveyListColumnConfig from '../../configs/surveyListColumnConfig';
import SurveysPageRender from "../../components/Survey/Survey";

const Surveys = ({
  surveyList, getSurveys, toggleSurveyActive, showLoader, surveyDetails, fetchSurveyDetails, markSurveyPrimary,
}) => {

  useEffect(() => {
    getSurveys();
  }, []);

  const markPrimaryAction = row => {
    markSurveyPrimary(row)
  }

  return (
    <SurveysPageRender
    surveyColumnConfig={makeSurveyListColumnConfig({toggleSurveyActive, markPrimaryAction})}
    surveyDetails={surveyDetails}
    surveyList={surveyList}
    surveyType="NORMAL"
    surveyRowClicked={fetchSurveyDetails}
    showLoader={showLoader}
    />
  );
};

const mapStateToProps = ({ surveys: { list, editing }, ui: { loading } }) => ({
  surveyList: list,
  showLoader: loading,
  surveyDetails: editing,
});

const mapDispatchToProps = dispatch => ({
  getSurveys: () => dispatch(fetchSurveys()),
  toggleSurveyActive: (surveyDetails) => dispatch(patchSurvey({ id: surveyDetails.id, active: !surveyDetails.active })),
  markSurveyPrimary: (surveyDetails) => dispatch(markPrimary({ id: surveyDetails.id, primary: true })),
  fetchSurveyDetails: (surveyDetails) => dispatch(fetchAndEditSurvey({ id: surveyDetails.id })),
});

export default connect(mapStateToProps, mapDispatchToProps)(Surveys);
