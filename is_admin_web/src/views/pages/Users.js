import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import User from '../../components/User';
import Spinner from '../../components/@IS/spinner/Loading-spinner';
import LoadingSpinner from '../../components/@IS/spinner/Fallback-spinner';
import { fetchUsers, fetchUserDetails } from '../../redux/users/actions';
import UserList from '../../components/@IS/list';
import makeUserListColumnConfig from '../../configs/userListColumnConfig';
import ModalBasic from '../../components/@IS/modal/Modal';


const Users = ({ getUsers, fetchUserData, userList, showLoader, userDetails }) => {

  const [showUserModal, updateShowUserModal] = useState(false)
  const [userDetailsLocal, updateUserDetailsLocal] = useState(userDetails)

  useEffect(() => {
    getUsers()
  }, [])

  useEffect(() => {
    if (userDetails) {
      updateUserDetailsLocal(userDetails);
      updateShowUserModal(true);
    }
  }, [userDetails])

  const onRowClicked = ({ email }) => {
    fetchUserData(email)
  }

  const closeUserModal = () => {
    updateShowUserModal(false)
  }

  return (
    <>
      {showLoader && userList ? <LoadingSpinner /> : null}
      {(!userList) ? <Spinner /> 
      : <UserList 
        data={userList} 
        columnConfig={makeUserListColumnConfig()}
        onRowClicked={onRowClicked} 
        noDataComponent='No Users'
      />}
      {showUserModal ? <ModalBasic isOpen={showUserModal} header={userDetailsLocal.name} onClose={closeUserModal}><User user={userDetailsLocal}/></ModalBasic> : null}
    </>
  );
};

const mapStateToProps = ({ users: { list, selected }, ui: { loading } }) => ({
  userList: list,
  showLoader: loading,
  userDetails: selected,
});


const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(fetchUsers()),
  fetchUserData: (userEmail) => {
    console.log(userEmail)
    dispatch(fetchUserDetails(userEmail))
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Users);
