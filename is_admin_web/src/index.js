import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';

import { store } from './redux/storeConfig/store';
import { Layout } from './utility/context/Layout';
import * as serviceWorker from './serviceWorker';
import './index.scss';
import 'react-toastify/dist/ReactToastify.css';
import App from './App';


ReactDOM.render(
  <Provider store={store}>
    <ToastContainer />
    <Layout>
      <App />
    </Layout>
  </Provider>,
  document.querySelector('#root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
