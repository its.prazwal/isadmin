import React from 'react';
import { Navbar } from 'reactstrap';

import NavbarUser from './NavbarUser';


const ThemeNavbar = (props) => (
  <>
    <div className="content-overlay" />
    <div className="header-navbar-shadow" />
    <Navbar
      className='header-navbar navbar-expand-lg navbar navbar-with-menu navbar-shadow navbar-light floating-nav'
    >
      <div className="navbar-wrapper">
        <div className="navbar-container content">
          <div
            className="navbar-collapse d-flex justify-content-end align-items-center"
            id="navbar-mobile"
          >
            <NavbarUser
              handleAppOverlay={props.handleAppOverlay} 
            />
          </div>
        </div>
      </div>
    </Navbar>
  </>
);


export default ThemeNavbar;
