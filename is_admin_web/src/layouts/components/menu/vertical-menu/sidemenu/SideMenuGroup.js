import React, { useState, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import { Badge } from 'reactstrap';
import classnames from 'classnames';
import { ChevronRight } from 'react-feather';

const SideMenuGroup =(props)=> {

  const [isOpen, setIsOpen] = useState(false);
  const [activeItem, setActiveItem] = useState();
  const prevProps = useRef();
  let flag = true;
  let parentArray = [];
  let childObj = {};

  useEffect(()=>{
    setActiveItem(props.activePath)
  },[])


  useEffect(()=>{
    prevProps.current = props;
  },[props])

  const handleActiveItem = (url) => {
    setActiveItem(url)
  };

  useEffect(() => {
    if (prevProps.activePath !== props.activePath) {
      if (childObj.navLink && childObj.collapsed) {
        props.collapsedMenuPaths(childObj.navLink);
      }
      if (
        props.activePath === childObj.navLink
        && !props.parentArr.includes(parentArray[0])
      ) {
        props.parentArr.splice(0, props.parentArr.length);
        props.parentArr.push(parentArray);
      } else if (props.parentArr.includes(parentArray)) {
        props.parentArr.splice(0, props.parentArr.length);
      }
    }
  }, [props.activePath, props.parentArr])

  const renderChild = (item, activeGroup, handleGroupClick, handleActiveItem, parent) => {
    return (
      <ul className="menu-content">
        {item.children
          ? item.children.map((child) => {
            const CustomAnchorTag = child.type === 'external-link' ? 'a' : Link;
            if (!parentArray.includes(item.id) && flag) {
              parentArray.push(item.id);
            }

            if (child.navlink && child.collapsed) {
              props.collapsedMenuPaths(child.navLink);
            }

            if (props.activeItemState === child.navLink) {
              childObj = child;
              props.parentArr.push(parentArray);
              flag = false;
            }
            if (
              (child.permissions
                  && child.permissions.includes(props.currentUser))
                || child.permissions === undefined
            ) {
              return (
                <li
                  key={child.id}
                  className={classnames({
                    hover: props.hoverIndex === child.id,
                    'has-sub': child.type === 'collapse',
                    open:
                        child.type === 'collapse'
                        && activeGroup.includes(child.id),
                    'sidebar-group-active': props.currentActiveGroup.includes(
                      child.id,
                    ),
                    active:
                        (props.activeItemState === child.navLink
                        && child.type === 'item') || (item.parentOf && item.parentOf.includes(props.activeItemState)),
                    disabled: child.disabled,
                  })}
                  onClick={(e) => {
                    e.stopPropagation();
                    handleGroupClick(child.id, item.id, child.type);
                    if (child.navLink) {
                      handleActiveItem(child.navLink);
                    }
                    if (props.deviceWidth <= 1200 && child.type === 'item') {
                      props.toggleMenu();
                    }
                  }}
                >
                  <CustomAnchorTag
                    className={classnames({
                      'd-flex justify-content-between': child.type === 'collapse',
                    })}
                    to={
                        child.navLink && child.type === 'item'
                          ? child.navLink
                          : ''
                      }
                    href={child.type === 'external-link' ? child.navLink : ''}
                    onMouseEnter={() => {
                      props.handleSidebarMouseEnter(child.id);
                    }}
                    onMouseLeave={() => {
                      props.handleSidebarMouseEnter(child.id);
                    }}
                    key={child.id}
                    onClick={(e) => (child.type === 'collapse'
                      ? e.preventDefault()
                      : '')}
                    target={child.newTab ? '_blank' : undefined}
                  >
                    <div className="menu-text">
                      {child.icon}
                      <span className="menu-item menu-title">
                        {child.title}
                      </span>
                    </div>
                    {child.badge ? (
                      <Badge
                        color={child.badge}
                        className="float-right mr-2"
                        pill
                      >
                        {child.badgeText}
                      </Badge>
                    ) : (
                      ''
                    )}
                    {child.type === 'collapse' ? (
                      <ChevronRight className="menu-toggle-icon" size={13} />
                    ) : (
                      ''
                    )}
                  </CustomAnchorTag>

                  {child.children
                    ? renderChild(
                      child,
                      activeGroup,
                      handleGroupClick,
                      handleActiveItem,
                      item.id,
                    )
                    : ''}
                </li>
              );
            } if (
              child.navLink === props.activePath
                && !child.permissions.includes(props.currentUser)
            ) {
              return props.redirectUnauthorized();
            }
            return null;
          })
          : null}
      </ul>
    );
  }

    return (
      <>
        {renderChild(
          props.group,
          props.activeGroup,
          props.handleGroupClick,
          props.handleActiveItem,
          null,
        )}
      </>
    );
}
export default SideMenuGroup;
