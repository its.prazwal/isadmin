import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import Hammer from 'react-hammerjs';
import PerfectScrollbar from 'react-perfect-scrollbar';

import { ContextLayout } from '../../../../utility/context/Layout';
import SidebarHeader from './SidebarHeader';
import SideMenuContent from './sidemenu/SideMenuContent';


const Sidebar = (props) => {

    const [width, setWidth] = useState(window.innerWidth)
    const [activeIndex, setActiveIndex] = useState(null)
    const [activeItem, setActiveItem] = useState(null)
    const [menuShadow, setMenuShadow] = useState(false)
    const [ScrollbarTag, setScrollbarTag] = useState(() => PerfectScrollbar)
  let mounted = false;

  useEffect(()=>{
    mounted = true;
    if (props.activePath !== activeItem) {
      setActiveItem(props.activePath)
    }else{
      setActiveItem(null)
    }
    if (mounted) {
      if (window !== 'undefined') {
        window.addEventListener('resize', updateWidth, false);
      }
      checkDevice();
    }
    return () => mounted = false;
  },[])


  const updateWidth = () => {
    if (mounted) {
      setWidth(window.innerWidth);
      checkDevice();
    }
  };

  const checkDevice = () => {
    const prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    const mq = function (query) {
      return window.matchMedia(query).matches;
    };

    // if ('ontouchstart' in window || window.DocumentTouch) {
    // setScrollbarTag('div')
    // } else {
    // setScrollbarTag(PerfectScrollbar)
    // }
    const query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
  };

  const changeActiveIndex = (id) => {
    if (id !== activeIndex) {
      setActiveIndex(id);
    } else {
      setActiveIndex(null)
    }
  };

  const handleActiveItem = (url) => {
    setActiveItem(url);
  };

    const {
      visibilityState,
      toggleSidebarMenu,
      toggle,
      color,
      sidebarVisibility,
      activeTheme,
      collapsed,
      activePath,
      sidebarState,
      currentLang,
      userPermissions,
      currentUser,
      collapsedMenuPaths,
    } = props;

    const scrollShadow = (container, dir) => {
      if (container && dir === 'up' && container.scrollTop >= 100) {
        setMenuShadow(true);
      } else if (container && dir === 'down' && container.scrollTop < 100) {
        setMenuShadow(false);
      } else {
      }
    };
    return (
      <ContextLayout.Consumer>
        {(context) => {
          const dir = context.direction;
          return (
            <>
              <Hammer
                onSwipe={(e) => {
                  sidebarVisibility();
                }}
                direction={
                  dir === 'rtl' ? 'DIRECTION_LEFT' : 'DIRECTION_RIGHT'
                }
              >
                <div className="menu-swipe-area d-xl-none d-block vh-100" />
              </Hammer>

              <div
                className={classnames(
                  `main-menu menu-fixed menu-light menu-accordion menu-shadow theme-${activeTheme}`,
                  {
                    collapsed: sidebarState === true,
                    'hide-sidebar':
                      width < 1200 && visibilityState === false,
                  },
                )}
              >
                <SidebarHeader
                  toggleSidebarMenu={toggleSidebarMenu}
                  toggle={toggle}
                  sidebarBgColor={color}
                  sidebarVisibility={sidebarVisibility}
                  activeTheme={activeTheme}
                  collapsed={collapsed}
                  menuShadow={menuShadow}
                  activePath={activePath}
                  sidebarState={sidebarState}
                />
                <ScrollbarTag
                  className={classnames('main-menu-content', {
                    'overflow-hidden': ScrollbarTag !== 'div',
                    'overflow-scroll': ScrollbarTag === 'div',
                  })}
                  {...(ScrollbarTag !== 'div' && {
                    options: { wheelPropagation: false },
                    onScrollDown: (container) => scrollShadow(container, 'down'),
                    onScrollUp: (container) => scrollShadow(container, 'up'),
                    onYReachStart: () => menuShadow === true
                      && setMenuShadow(false),
                  })}
                >
                  <Hammer
                    onSwipe={() => {
                      sidebarVisibility();
                    }}
                    direction={
                      dir === 'rtl' ? 'DIRECTION_RIGHT' : 'DIRECTION_LEFT'
                    }
                  >
                    <ul className="navigation navigation-main">
                      <SideMenuContent
                        setActiveIndex={changeActiveIndex}
                        activeIndex={activeIndex}
                        activeItemState={activeItem}
                        handleActiveItem={handleActiveItem}
                        activePath={activePath}
                        lang={currentLang}
                        userPermissions={userPermissions}
                        currentUser={currentUser}
                        collapsedMenuPaths={collapsedMenuPaths}
                        toggleMenu={sidebarVisibility}
                        deviceWidth={props.deviceWidth}
                      />
                    </ul>
                  </Hammer>
                </ScrollbarTag>
              </div>
            </>
          );
        }}
      </ContextLayout.Consumer>
    );
}

const mapStateToProps = ({auth}) => ({
  currentUser: auth.user,
});

export default connect(mapStateToProps)(Sidebar);
