import React, { useState, useEffect, useRef } from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';

import Customizer from '../components/@IS/customizer/Customizer';
import Sidebar from './components/menu/vertical-menu/Sidebar';
import Navbar from './components/navbar/Navbar';
import {
  changeMode,
  collapseSidebar,
  changeNavbarColor,
  changeNavbarType,
  changeMenuColor,
  hideScrollToTop,
} from '../redux/customizer/actions';

const VerticalLayout = (props) => {

  const [width,setWidth] =  useState(window.innerWidth);
  const [sidebarState,setSidebarState] =  useState(props.customizer?.sidebarCollapsed);
  const [layout,setLayout] =  useState(props.customizer?.theme);
  const [collapsedContent, setCollapsedContent] =  useState(props.customizer?.sidebarCollapsed);
  const [sidebarHidden, setSidebarHidden] =  useState(false);
  const [customizer, setCustomizer] =  useState(false);
  const [currRoute, setCurrRoute] =  useState(props.location.pathname || '');
  const prevProps = useRef();
  let collapsedPaths = [];
  let mounted = false;

  useEffect(()=>{
    mounted = true;
    const {
      location: { pathname },
      customizer: { theme },
    } = props;

    if (mounted) {
      if (window !== 'undefined') {
        window.addEventListener('resize', updateWidth, false);
      }
      if (collapsedPaths.includes(pathname)) {
        props.collapseSidebar(true);
      }
      let layout = theme;
      return layout === 'dark'
          ? document.body.classList.add('dark-layout')
          : (layout === "semi-dark"
              ? document.body.classList.add("semi-dark-layout")
              : null);
    }

    return () => mounted = false;
  },[])

  useEffect(()=>{
    prevProps.current = props
  },[props])

  const updateWidth = () => {
    if (mounted){
        setWidth(window.innerWidth)
    }
  };

  const handleCustomizer = (bool) => {
    setCustomizer(bool);
  };

  useEffect(() => {
    const {
      location: { pathname },
        customizer: { theme, sidebarCollapsed },
    } = props;

    const layout = theme;
    if (mounted) {
      if (layout === 'dark') {
        document.body.classList.remove('semi-dark-layout');
        document.body.classList.add('dark-layout');
      }
      if (layout === 'semi-dark') {
        document.body.classList.remove('dark-layout');
        document.body.classList.add('semi-dark-layout');
      }
      if (layout !== 'dark' && layout !== 'semi-dark') {
        document.body.classList.remove('dark-layout', 'semi-dark-layout');
      }

      if (
        prevProps.customizer?.sidebarCollapsed
        !== props.customizer?.sidebarCollapsed
      ) {
        setCollapsedContent(sidebarCollapsed);
        setSidebarState(sidebarCollapsed)

      }
      if (
        prevProps.customizer?.sidebarCollapsed
        === props.customizer?.sidebarCollapsed
        && pathname !== prevProps.location.pathname
        && collapsedPaths.includes(pathname)
      ) {
        props.collapseSidebar(true);
      }
      if (
        prevProps.customizer?.sidebarCollapsed
        === props.customizer?.sidebarCollapsed
        && pathname !== prevProps.location.pathname
        && !collapsedPaths.includes(pathname)
      ) {
        props.collapseSidebar(false);
      }
    }
  })

  const handleCollapsedMenuPaths = (item) => {
    if (!collapsedPaths.includes(item)) {
      collapsedPaths = collapsedPaths.push(item);
    }
  };

  const toggleSidebarMenu = (val) => {
    setSidebarState(!sidebarState)
    setCollapsedContent(!collapsedContent)
  };

  const sidebarMenuHover = (val) => {
    setSidebarState(val);
  };

  const handleSidebarVisibility = () => {
    if (mounted) {
      if (window !== undefined) {
        window.addEventListener('resize', () => {
          if (sidebarHidden) {
            setSidebarHidden(!sidebarHidden)
          }
        });
      }
      setSidebarHidden(!sidebarHidden)
    }
  };


    const appProps = props?.customizer;
    const menuThemeArr = [
      'primary',
      'success',
      'danger',
      'info',
      'warning',
      'dark',
    ];
    const sidebarProps = {
      toggleSidebarMenu: props.collapseSidebar,
      toggle: toggleSidebarMenu,
      sidebarState: sidebarState,
      sidebarHover: sidebarMenuHover,
      sidebarVisibility: handleSidebarVisibility,
      visibilityState: sidebarHidden,
      activePath: props.match.path,
      collapsedMenuPaths: handleCollapsedMenuPaths,
      activeTheme: appProps.menuTheme,
      collapsed: collapsedContent,
      userPermissions: props.userPermissions,
      deviceWidth: width,
    };

    const customizerProps = {
      customizerState: customizer,
      handleCustomizer: handleCustomizer,
      changeMode: props.changeMode,
      changeNavbar: props.changeNavbarColor,
      changeNavbarType: props.changeNavbarType,
      changeMenuTheme: props.changeMenuColor,
      collapseSidebar: props.collapseSidebar,
      hideScrollToTop: props.hideScrollToTop,
      activeMode: appProps.theme,
      activeNavbar: appProps.navbarColor,
      navbarType: appProps.navbarType,
      menuTheme: appProps.menuTheme,
      scrollToTop: appProps.hideScrollToTop,
      sidebarState: appProps.sidebarCollapsed,
    };
    return (
      <div
        className={classnames(
          `wrapper vertical-layout theme-${appProps.menuTheme}`,
          {
            'menu-collapsed':
              collapsedContent === true && width >= 1200,
            'navbar-static': appProps.navbarType === 'static',
            'navbar-sticky': appProps.navbarType === 'sticky',
            'navbar-floating': appProps.navbarType === 'floating',
            'navbar-hidden': appProps.navbarType === 'hidden',
            'theme-primary': !menuThemeArr.includes(appProps.menuTheme),
          },
        )}
      >
        <Sidebar {...sidebarProps} />
        <div
          className='app-content content'
        >
          <Navbar/>
          <div className="content-wrapper">{props.children}</div>
        </div>

        {appProps.disableCustomizer !== true ? (
          <Customizer {...customizerProps} />
        ) : null}
        <div
          className="sidenav-overlay"
          onClick={handleSidebarVisibility}
        />
      </div>
    );
}
const mapStateToProps = ({customizer}) => ({
  customizer
});
export default connect(mapStateToProps, {
  changeMode,
  collapseSidebar,
  changeNavbarColor,
  changeNavbarType,
  changeMenuColor,
  hideScrollToTop,
})(VerticalLayout);
